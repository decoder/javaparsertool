#!/bin/bash
helpFunction()
{
   echo "Usage: $0 [-p port (Default 5003)]"
   echo "\t-p server port"
   echo "\n"
   exit 1 # Exit script after printing help
}

while getopts ":hp:" opt
do
   case "$opt" in
   	  h ) echo "\n";helpFunction ;;
      p ) port="$OPTARG" ;;
      ? ) echo "\nInvalid option";helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$port" ]
then
  port=5003
fi

export FLASK_APP=restService.py
export FLASK_DEBUG=1
export FLASK_ENV=development 
flask run --host=0.0.0.0 --port=$port --debugger --reload