#!flask/bin/python
from flask import Flask, jsonify, abort
import os, sys
from flask import request
import random, string, json
import time
import subprocess

app = Flask(__name__)
app.debug = True
app.reloader = True
INV_ID=None

def printToLog(stringToPrint, initialize="a"):
	global INV_ID
	text_file = open("./logs/"+INV_ID+".log", initialize)
	text_file.write(stringToPrint+"\n")
	text_file.close()

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

@app.route('/decoder/classModelToJson/<dbName>/<path:sourceFileName>', methods=['GET'])
def runClassModel2JSON(dbName, sourceFileName):
	global INV_ID
	if (not ('key' in request.headers)) or (not ("invocationID" in request.args)):
		abort(400)

	key = request.headers.get('key')
	invocationID=request.args["invocationID"]
	INV_ID = invocationID
	printToLog("", "w")

	orderToRun = "python3 process.py --key " + key + " --dbName " + dbName + " --sourceFileName " + sourceFileName + " --invocationId " + invocationID

	printToLog(orderToRun)

	output = "res_" + invocationID

	p = subprocess.Popen(orderToRun.split(" "))
	p.wait()

	f = open(output,'r')
	outputFileContent=json.load(f)
	printToLog(json.dumps(outputFileContent, indent=4))
	if str(outputFileContent['code'])[0] != "2":
		#os.system("rm " + output)
		abort(outputFileContent['code'])
	else:
		#os.system("rm " + output)
		return outputFileContent['answer']