# !/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import string
import argparse
import requests
import json
import urllib3
import random
from datetime import datetime
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from urllib.parse import quote

def loadFileContent(fileName):
	with open(fileName, 'r') as file:
		data = file.read()
		return data

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	if username==None  and password==None:
		pload = {"user_name":"admin", "user_password":"cN6jK7eU5bM3"}
	else:
		pload = {"user_name":username, "user_password":password}
	url="/user/login"
	resStatus, resLogin=doPOST(url,str(pload).replace("'",'"'))
	key=(json.loads(resLogin))['key']
	return key



def doPOST(url, data, key=None):
	r = requests.post('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def updateInvocation(invocationUpdate):
	global dbName, key
	getSt, getTx=doGET('/invocations/'+dbName+'/'+invocationUpdate['invocationID'], key)
	print(getSt,getTx)
	inv=json.loads(getTx)

	inv.update(invocationUpdate)
	doPUT('/invocations/'+dbName, json.dumps([inv]), key)
	print(json.dumps([inv], indent=4))


PKM_API_HOST="pkm"

#START PARAMETERS
ap = argparse.ArgumentParser(description="Decoder OpenJML Process",add_help=False)
ap.add_argument("-k", "--key", required=True, metavar="login key")
ap.add_argument("-d", "--dbName", required=False, metavar="database name", default="mythaistar")
ap.add_argument("-s", "--sourceFileName", required=True, metavar="Source File name to parse")
ap.add_argument("-i", "--invocationId", required=True, metavar="The Invocation ID")
args = vars(ap.parse_args())

key=args['key']
dbName=args['dbName']
fileName=args['sourceFileName']
fileName2=fileName.split("/")[-1]
invocationId=args['invocationId']
outputLogFile="res_"+invocationId
#END PARAMETERS

url="/uml/rawuml/"+dbName+"/"+quote(fileName, safe='')
status,document=doGET(url,key)
document=json.loads(document)
print(document)
text_file = open(fileName2, "w")
text_file.write(document['content'])
text_file.close()

classModel2JSONCommand="java -jar ClassModelXMI2JSON.jar /classmodelxmi2jsontransformer/"+fileName2
os.system(classModel2JSONCommand)

classModelJSON2MermaidCommand="java -jar ClassModelJSON2Mermaid.jar /classmodelxmi2jsontransformer/"+fileName2.replace(".uml",".json")
os.system(classModelJSON2MermaidCommand)

data=loadFileContent(fileName2.replace(".uml",".json"))
d=json.loads(data)
if isinstance(d,list):
	d=d[0]
d["diagram"]=loadFileContent(fileName2.replace(".uml","Mermaid.json"))
d['name']=fileName
d['sourcefile']=fileName.replace(".uml",".java")
d['type']="UML Model"
data=json.dumps([d])
resultsToAdd=[]
doPutAnswerCode,doPutAnswer  = doPUT('/uml/uml_class/'+dbName, data, key)

rmCommand = "rm "+fileName2+" "+fileName2.replace(".uml",".json")+" "+fileName2.replace(".uml","Mermaid.json")
#os.system(rmCommand)

invocationUpdate=dict()
invocationUpdate["invocationID"] = invocationId
invocationUpdate["invocationStatus"]="COMPLETED"
invocationUpdate["timestampCompleted"]=datetime.now().strftime("%Y%m%d_%H%M%S")
invocationResults=resultsToAdd
invocationResults.append(dict(path='/uml/uml_class/'+dbName+"/"+str(fileName),type="diagram"))
invocationUpdate["invocationResults"]=invocationResults
updateInvocation(invocationUpdate)



outputLog = open(outputLogFile, "w")
outputLog.write(json.dumps(dict(code=doPutAnswerCode,answer=doPutAnswer), indent=4))
outputLog.close()