#!flask/bin/python
from flask import Flask, jsonify, abort
import os, sys
from flask import request
import random, string, json
from requests.utils import quote
import subprocess

app = Flask(__name__)
app.debug = True
app.reloader = True

INV_ID=None

def printToLog(stringToPrint, initialize="a"):
	global INV_ID
	text_file = open("./logs/"+INV_ID+".log", initialize)
	text_file.write(str(stringToPrint)+"\n")
	text_file.close()

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

@app.route('/decoder/javaASTGenerator/<dbName>', methods=['GET'])
def runASTExtractorWholeProject(dbName):
	global INV_ID
	
	if (not ('key' in request.headers)) or (not ("invocationID" in request.args)):
		abort(400)

	key = request.headers.get('key')
	invocationID=request.args["invocationID"]
	INV_ID = invocationID
	printToLog("","w")

	generate="all"
	if "generate" in request.args:
		if request.args['generate'] == "ast":
			generate="ast"
		elif request.args['generate'] == "comments":
			generate="comm"
		elif request.args['generate'] == "annotations":
			generate="anno"
		elif request.args['generate'] == "all":
			generate="all"
		else:
			abort(400)

	

	orderToRun = "python3 process.py --key " + key + " --dbName " + dbName + " --generate " + generate + " --invocationId " + invocationID
	p = subprocess.Popen(orderToRun.split(" "))
	p.wait()

	printToLog(orderToRun, "w")

	output = "res_" + invocationID
	
	f = open(output,'r')
	outputFileContent=json.load(f)
	printToLog(outputFileContent)
	os.system("rm " + output)
	answers=[]
	for entry in outputFileContent:
		if str(entry['code'])[0] != "2":
			abort(entry['code'])
		else:
			answers.append(entry['answer'])
	os.system("rm " + output)
	printToLog(json.dumps(answers, indent=4))
	return json.dumps(dict(answers=answers), indent=4)
	
	
@app.route('/decoder/javaASTGenerator/<dbName>/<path:sourceFileName>', methods=['GET'])
def runASTExtractor(dbName, sourceFileName):
	global INV_ID
	
	if (not ('key' in request.headers)) or (not ("invocationID" in request.args)):
		abort(400)

	key = request.headers.get('key')
	invocationID=request.args["invocationID"]
	INV_ID = invocationID
	printToLog("","w")

	generate="all"
	if "generate" in request.args:
		if request.args['generate'] == "ast":
			generate="ast"
		elif request.args['generate'] == "comments":
			generate="comm"
		elif request.args['generate'] == "annotations":
			generate="anno"
		elif request.args['generate'] == "all":
			generate="all"
		else:
			abort(400)

	

	orderToRun = "python3 process.py --key " + key + " --dbName " + dbName + " --sourceFileName " + quote(sourceFileName, safe='') + " --generate " + generate + " --invocationId " + invocationID
	p = subprocess.Popen(orderToRun.split(" "))
	p.wait()

	printToLog(orderToRun, "w")

	output = "res_" + invocationID
	
	f = open(output,'r')
	outputFileContent=json.load(f)
	printToLog(outputFileContent)
	os.system("rm " + output)
	answers=[]
	for entry in outputFileContent:
		if str(entry['code'])[0] != "2":
			abort(entry['code'])
		else:
			answers.append(entry['answer'])
	os.system("rm " + output)
	printToLog(json.dumps(answers, indent=4))
	return json.dumps(dict(answers=answers), indent=4)
	
	


