# !/usr/bin/python
# -*- coding: utf-8 -*- 


import requests
import urllib3
import random, string
from urllib.parse import quote


from datetime import datetime


import os
import sys
import json
import string
import argparse

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PKM_API_HOST="pkm"


def loadFileContent(fileName):
	with open(fileName, 'r') as file:
		data = file.read()
		return data

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	if username==None  and password==None:
		pload = {"user_name":"admin", "user_password":"cN6jK7eU5bM3"}
	else:
		pload = {"user_name":username, "user_password":password}
	url="/user/login"
	resStatus, resLogin=doPOST(url,str(pload).replace("'",'"'))
	key=(json.loads(resLogin))['key']
	return key

def getDepth(x):
    if type(x) is dict and x:
        return 1 + max(getDepth(x[a]) for a in x)
    if type(x) is list and x:
        return 1 + max(getDepth(a) for a in x)
    return 0

def doPOST(url, data, key=None):	
	r = requests.post('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def updateInvocation(invocationUpdate):
	global dbName, key
	getSt, getTx=doGET('/invocations/'+dbName+'/'+invocationUpdate['invocationID'], key)
	
	inv=json.loads(getTx)
	print(inv)
	inv.update(invocationUpdate)
	putST, putTX = doPUT('/invocations/'+dbName, json.dumps([inv]), key)
	
	return putST, putTX

	

def sendLogAndFinishProcess(status, logMessages, warnings, errors, startTimestamp, resPutsAnswers, resultsToAdd):
	global dbName, key, makeOutputLogFile, outputLogFile, invocationId
	formatoFecha="%a, %d %b %Y %H:%M:%S %Z%z"
	logRes=dict(tool="JavaParser", details=dict(numberOfErrors=0, numberOfWarings=0), status=status, type="Log", messages=logMessages, warnings=warnings, errors=errors)
	logRes["nature of report"]="JavaParser report"
	logRes["start running time"]=startTimestamp.strftime(formatoFecha)
	logRes["end running time"]=datetime.now().strftime(formatoFecha)
	
	invocationUpdate=dict()
	invocationUpdate["invocationID"] = invocationId
	postStatus,postText=doPOST('/log/'+dbName, json.dumps([logRes]), key)
	
	if int(postStatus)==201:
		postTextAsJSON=json.loads(postText)
		if status:
			invocationUpdate["invocationStatus"]="COMPLETED"
		else:
			invocationUpdate["invocationStatus"]="FAILED"
			invocationUpdate["invocationRes"]=dict(path="FAILED", type="message")

		if len(postTextAsJSON)>0:
			newLogID=postTextAsJSON[0]
			invocationResults=resultsToAdd
			invocationResults.append(dict(path="/log/"+dbName+"/"+str(newLogID),type="log"))
			invocationUpdate["invocationResults"]=invocationResults


		if resPutsAnswers!=None:
			if makeOutputLogFile:
				outputLog = open(outputLogFile, "w")
				outputLog.write(json.dumps(resPutsAnswers, indent=4))
				outputLog.close()
			else:
				print(resPutsAnswers)

		#if downloadedFileName!=None:
		#	rmCommand = "rm " + downloadedFileName + "_*.json " + downloadedFileName + "*.java "
			#os.system(rmCommand)

	else:
		invocationUpdate["invocationStatus"]="FAILED"
		invocationUpdate["invocationRes"]=dict(path="FAILED", type="message")

	invocationUpdate["timestampCompleted"]=datetime.now().strftime("%Y%m%d_%H%M%S")

	updateInvocation(invocationUpdate)

def processCommentFile(data):
	for i in range(len(data[0]['comments'])):
		if data[0]["comments"][i]['loc']['pos_start']['pos_lnum'] < data[0]["comments"][i]['loc']['pos_end']['pos_lnum']:
			data[0]["comments"][i]['loc']['pos_end']['pos_lnum'] = data[0]["comments"][i]['loc']['pos_end']['pos_lnum'] - 1
	return data

def processAnnotationFile(data):
	for i in range(len(data[0]['annotations'])):
		if getDepth(data[0]["annotations"][i]['jml'])>150:
			data[0]["annotations"][i]['jml']=str(data[0]["annotations"][i]['jml'])
		if data[0]["annotations"][i]['loc']['pos_start']['pos_lnum'] < data[0]["annotations"][i]['loc']['pos_end']['pos_lnum']:
			data[0]["annotations"][i]['loc']['pos_end']['pos_lnum'] = data[0]["annotations"][i]['loc']['pos_end']['pos_lnum'] - 1
	return data


def runASTAndJML2JSON(fileName, logMessages, document):
	downloadedFileName=fileName.replace(".java","")

	astExtractorCommand="java -jar ASTExtractor.jar -file=" + fileName+"; mv " + downloadedFileName + "_annotations.json " + downloadedFileName + "_annotations_temp.json"
	os.system(astExtractorCommand)
	logMessages.append(downloadedFileName+" AST EXTRACTOR success!")

	jmlCommand = "java -jar JML2JSON.jar " + downloadedFileName + "_annotations_temp.json " + downloadedFileName + "_annotations.json"
	os.system(jmlCommand)
	logMessages.append(downloadedFileName+" JML2JSON success!")

	data=loadFileContent(downloadedFileName+"_proc.java")
	
	document['content'] = data
	documentToUpload=[]
	documentToUpload.append(document)

	doPUT("/code/rawsourcecode/"+dbName, json.dumps(documentToUpload,indent=2), key)
	logMessages.append(downloadedFileName+" Uploaded New RawSourceCode Normalized Version")

	return logMessages

def uploadFile(dbName, sf, resultsToAdd, resPutsAnswers, logMessages, collection, data):
	resultsToAdd.append(dict(path='/code/java/'+collection+'/'+dbName+"/"+sf, type=collection))
	scPutCode, scPut = doPUT('/code/java/'+collection+'/'+dbName, json.dumps(data,indent=2), key)
	resPutsAnswers.append(dict(code=scPutCode, answer=scPut))
	logMessages.append(sf+" Uploaded AST")
	return resultsToAdd, resPutsAnswers, logMessages



def processFile(document, fileName, generate, resultsToAdd, resPutsAnswers, logMessages):
	text_file = open(quote(fileName, safe=''), "wt")
	text_file.write(document['content'])
	text_file.close()

	logMessages = runASTAndJML2JSON(quote(fileName, safe=''), logMessages, document)

	if generate == "all" or generate == "ast":
		data=json.loads(loadFileContent(quote(fileName, safe='').replace(".java","")+"_ast.json"))
		data[0]['sourceFile']=data[0]['sourceFile'].replace("%2F","/")
		
		resultsToAdd, resPutsAnswers, logMessages = uploadFile(dbName, data[0]['sourceFile'], resultsToAdd, resPutsAnswers, logMessages, "sourcecode", data)

	if generate == "all" or generate == "comm":
		data=json.loads(loadFileContent(quote(fileName, safe='').replace(".java","")+"_comments.json"))
		data[0]['sourceFile']=data[0]['sourceFile'].replace("%2F","/")
		data=processCommentFile(data)

		resultsToAdd, resPutsAnswers, logMessages = uploadFile(dbName, data[0]['sourceFile'], resultsToAdd, resPutsAnswers, logMessages, "comments", data)

	if generate == "all" or generate == "anno":
		data=json.loads(loadFileContent(quote(fileName, safe='').replace(".java","")+"_annotations.json"))
		data[0]['sourceFile']=data[0]['sourceFile'].replace("%2F","/")
		processAnnotationFile(data)

		resultsToAdd, resPutsAnswers, logMessages = uploadFile(dbName, data[0]['sourceFile'], resultsToAdd, resPutsAnswers, logMessages, "annotations", data)

	#sendLogAndFinishProcess(status, logMessages, [], [], startTimestamp, resPutsAnswers, downloadedFileName, resultsToAdd)
	return resultsToAdd, resPutsAnswers, logMessages



startTimestamp=datetime.now()

#START PARAMETERS
ap = argparse.ArgumentParser(description="Decoder JavaParser Process",add_help=False)
ap.add_argument("-k", "--key", required=True, metavar="login key")
ap.add_argument("-d", "--dbName", required=False, metavar="database name", default="mythaistar")
ap.add_argument("-s", "--sourceFileName", required=False, metavar="Source File name to parse", default=None)
ap.add_argument("-g", "--generate", required=False, metavar="Generation Items [all | ast | comm | anno]", default="all", choices=['all','ast','comm','anno'])
ap.add_argument("-i", "--invocationId", required=True, metavar="The Invocation ID")
args = vars(ap.parse_args())

key=args['key']
dbName=args['dbName']

fileName=None
if 'sourceFileName' in args:
	fileName=args['sourceFileName']

generate=args['generate']

invocationId=args['invocationId']
outputLogFile="res_"+invocationId
#END PARAMETERS

makeOutputLogFile=True
if outputLogFile==None:
	makeOutputLogFile=False


if fileName!=None:
	url="/code/rawsourcecode/"+dbName+"/"+fileName
else:
	url="/code/rawsourcecode/"+dbName

getStatus,allDocuments=doGET(url,key)

if getStatus!=200:
	print("Error downloading file")
	sendLogAndFinishProcess(False, ["Error downloading file"], [], [], startTimestamp, None, None,[])
	quit()

else:
	logMessages=[]
	resPutsAnswers=[]
	resultsToAdd=[]
	logMessages.append("Downloaded Succesfully!")
	allDocuments=json.loads(allDocuments)

	if not (isinstance(allDocuments, list)):
		allDocuments=[allDocuments]

	for document in allDocuments:
		if fileName!=None:
			fn = fileName
		else:
			fn = document['rel_path']
		if fn.endswith('.java'):	
			resultsToAdd, resPutsAnswers, logMessages = processFile(document, fn, generate, resultsToAdd, resPutsAnswers, logMessages)

	os.system("rm *.java; rm *.json")
	sendLogAndFinishProcess(True, logMessages, [], [], startTimestamp, resPutsAnswers, resultsToAdd)
	
