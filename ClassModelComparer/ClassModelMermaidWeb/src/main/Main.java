package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONObject;

import inputoutput.TextFile;
import transformer.Transformer;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		String xmi,workingDir,fileURL,everything, xmiDiff, everything2;		

			workingDir = System.getProperty("user.dir");
			String fileName=workingDir+File.separator+args[0];
			//xmi=TextFile.readTextFile(fileName);
			
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			try {
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				
				while(line!=null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				everything = sb.toString();
				} finally {
					br.close();
			}
			
			String fileNameDiff=workingDir+File.separator+args[2];
			xmiDiff=TextFile.readTextFile(fileNameDiff);
			
			String fileName2=workingDir+File.separator+args[1];
			
			BufferedReader br2 = new BufferedReader(new FileReader(fileName2));
			try {
				StringBuilder sb = new StringBuilder();
				String line = br2.readLine();
				
				while(line!=null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br2.readLine();
				}
				everything2 = sb.toString();
				} finally {
					br2.close();
			}
		
        Transformer tr=new Transformer (everything,everything2,xmiDiff);
        String json=tr.transform();
        
        if(Config.debugMode){
        	System.out.println(json);
        	String targetFile = fileURL.substring(0,fileURL.lastIndexOf("."))+".json";
        	TextFile.writeTextFile(json, targetFile);
        }else{
        	String targetFile=workingDir+File.separator+args[0].substring(0, args[0].lastIndexOf("."))+".txt";
        	TextFile.writeTextFile(json, targetFile);
        }	                        

	}

}
