package inputoutput;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TextFile {
	public static String readTextFile(String path) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(path));
		
		String linea;
		StringBuffer datos=new StringBuffer();
		while ((linea = bf.readLine())!=null) {
			datos.append(linea);
		}
		bf.close();
		
		return datos.toString();
	}
	
	public static void writeTextFile(String content, String path) throws FileNotFoundException, UnsupportedEncodingException {
		 PrintWriter writer = new PrintWriter(path, "UTF-8");
		 writer.print(content);
		 writer.close();
	}
}
