package transformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Transformer {

	private String InputFile, InputDiff, InputFile2;
	private int PRETTY_PRINT_INDENT_FACTOR = 4;

	public Transformer(String mermaidDiagram, String mermaidDiagram2, String xmiDiff) {
		InputFile = mermaidDiagram;
		InputFile2 = mermaidDiagram2;
		InputDiff = xmiDiff;
	}

	public String transform(){
		
		JSONArray classNamesDel = new JSONArray();
		JSONArray classNamesCre = new JSONArray();
		JSONArray classNamesUpd = new JSONArray();
		
		JSONArray intNamesDel = new JSONArray();
		JSONArray intNamesCre = new JSONArray();
		JSONArray intNamesUpd = new JSONArray();
		
		JSONArray enumNamesDel = new JSONArray();
		JSONArray enumNamesCre = new JSONArray();
		JSONArray enumNamesUpd = new JSONArray();
		
		JSONArray classNamesAttDel = new JSONArray();
		JSONArray classNamesAttNDel = new JSONArray();
		JSONArray classNamesAttTDel = new JSONArray();
		
		JSONArray classNamesOprDel = new JSONArray();
		JSONArray classNamesOprNDel = new JSONArray();
		JSONArray classNamesOprRTDel = new JSONArray();
		
		JSONArray classNamesOprParDel = new JSONArray();
		JSONArray classNamesOprParNDel = new JSONArray();
		JSONArray classNamesOprParTDel = new JSONArray();
		
		JSONArray classNamesAttCre = new JSONArray();
		JSONArray classNamesAttNCre = new JSONArray();
		JSONArray classNamesAttTCre = new JSONArray();
		
		JSONArray classNamesOprCre = new JSONArray();
		JSONArray classNamesOprNCre = new JSONArray();
		JSONArray classNamesOprRTCre = new JSONArray();
		
		JSONArray classNamesOprParCre = new JSONArray();
		JSONArray classNamesOprParNCre = new JSONArray();
		JSONArray classNamesOprParTCre = new JSONArray();
		
		JSONArray classNamesAttUpd = new JSONArray();
		JSONArray classNamesAttNUpd = new JSONArray();
		JSONArray classNamesAttTUpd = new JSONArray();
		
		JSONArray classNamesOprUpd = new JSONArray();
		JSONArray classNamesOprNUpd = new JSONArray();
		JSONArray classNamesOprRTUpd = new JSONArray();
		
		JSONArray classNamesOprParUpd = new JSONArray();
		JSONArray classNamesOprParNUpd = new JSONArray();
		JSONArray classNamesOprParTUpd = new JSONArray();
		
		JSONArray classNames = new JSONArray();
		JSONArray intNames = new JSONArray();
		JSONArray enumNames = new JSONArray();
		
		JSONArray intNamesAttDel = new JSONArray();
		JSONArray intNamesAttNDel = new JSONArray();
		JSONArray intNamesAttTDel = new JSONArray();
		
		JSONArray intNamesOprDel = new JSONArray();
		JSONArray intNamesOprNDel = new JSONArray();
		JSONArray intNamesOprRTDel = new JSONArray();
		
		JSONArray intNamesOprParDel = new JSONArray();
		JSONArray intNamesOprParNDel = new JSONArray();
		JSONArray intNamesOprParTDel = new JSONArray();
		
		JSONArray intNamesAttCre = new JSONArray();
		JSONArray intNamesAttNCre = new JSONArray();
		JSONArray intNamesAttTCre = new JSONArray();
		
		JSONArray intNamesOprCre = new JSONArray();
		JSONArray intNamesOprNCre = new JSONArray();
		JSONArray intNamesOprRTCre = new JSONArray();
		
		JSONArray intNamesOprParCre = new JSONArray();
		JSONArray intNamesOprParNCre = new JSONArray();
		JSONArray intNamesOprParTCre = new JSONArray();
		
		JSONArray intNamesAttUpd = new JSONArray();
		JSONArray intNamesAttNUpd = new JSONArray();
		JSONArray intNamesAttTUpd = new JSONArray();
		
		JSONArray intNamesOprUpd = new JSONArray();
		JSONArray intNamesOprNUpd = new JSONArray();
		JSONArray intNamesOprRTUpd = new JSONArray();
		
		JSONArray intNamesOprParUpd = new JSONArray();
		JSONArray intNamesOprParNUpd = new JSONArray();
		JSONArray intNamesOprParTUpd = new JSONArray();
		
		JSONArray enumNamesLitDel = new JSONArray();
		JSONArray enumNamesLitCre = new JSONArray();
		JSONArray enumNamesLitUpd = new JSONArray();
		
		JSONArray assoNamesDel = new JSONArray();
		JSONArray assoNamesCre = new JSONArray();
		JSONArray assoNamesUpd = new JSONArray();
		
		JSONArray diferencias = new JSONArray(InputDiff);
		
		for(int i=0;i<diferencias.length();i++) {
			JSONObject obj = diferencias.getJSONObject(i);
			if(obj.has("classes")) {
				JSONArray arr = obj.getJSONArray("classes");
				for(int j = 0; j<arr.length(); j++) {
					JSONObject obj2 = arr.getJSONObject(j);
					
					if(obj2.getString("OperationType").equals("Delete")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("ClassAttribute")) {
							
							for(int z=0; z<classNamesAttDel.length();z++) {
								JSONObject attDel = classNamesAttDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeName")) {
							for(int z=0; z<classNamesAttNDel.length();z++) {
								JSONObject attDel = classNamesAttNDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttNDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeType")){
							for(int z=0; z<classNamesAttTDel.length();z++) {
								JSONObject attDel = classNamesAttTDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								classNamesAttTDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperation")) {
							
							for(int z=0; z<classNamesOprDel.length();z++) {
								JSONObject oprDel = classNamesOprDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationName")) {
							for(int z=0; z<classNamesOprNDel.length();z++) {
								JSONObject oprDel = classNamesOprNDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprNDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationReturnType")) {
							for(int z=0; z<classNamesOprRTDel.length();z++) {
								JSONObject oprDel = classNamesOprRTDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprRTDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameter")) {
							for(int z=0; z<classNamesOprParDel.length();z++) {
								JSONObject oprDel = classNamesOprParDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterName")) {
							for(int z=0; z<classNamesOprParNDel.length();z++) {
								JSONObject oprDel = classNamesOprParNDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParNDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterType")) {
							for(int z=0; z<classNamesOprParTDel.length();z++) {
								JSONObject oprDel = classNamesOprParTDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								classNamesOprParTDel.put(oprDel);
							}
						}
						else classNamesDel.put(obj2.getString("ElementName"));
					}
					
					else if(obj2.getString("OperationType").equals("Create")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("ClassAttribute")) {
							
							for(int z=0; z<classNamesAttCre.length();z++) {
								JSONObject attDel = classNamesAttCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeName")) {
							for(int z=0; z<classNamesAttNCre.length();z++) {
								JSONObject attDel = classNamesAttNCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttNCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeType")){
							for(int z=0; z<classNamesAttTCre.length();z++) {
								JSONObject attDel = classNamesAttTCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								classNamesAttTCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperation")) {
							
							for(int z=0; z<classNamesOprCre.length();z++) {
								JSONObject oprDel = classNamesOprCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationName")) {
							for(int z=0; z<classNamesOprNCre.length();z++) {
								JSONObject oprDel = classNamesOprNCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprNCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationReturnType")) {
							for(int z=0; z<classNamesOprRTCre.length();z++) {
								JSONObject oprDel = classNamesOprRTCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprRTCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameter")) {
							for(int z=0; z<classNamesOprParCre.length();z++) {
								JSONObject oprDel = classNamesOprParCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterName")) {
							for(int z=0; z<classNamesOprParNCre.length();z++) {
								JSONObject oprDel = classNamesOprParNCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParNCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterType")) {
							for(int z=0; z<classNamesOprParTCre.length();z++) {
								JSONObject oprDel = classNamesOprParTCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								classNamesOprParTCre.put(oprDel);
							}
						}
						else classNamesCre.put(obj2.getString("ElementName"));
					}
					else if(obj2.getString("OperationType").equals("Update")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("ClassName")) {
							
							classNames.put(obj2.getString("ElementName"));
						}
						
						else if(obj2.getString("ElementType").equals("ClassAttribute")) {
							
							for(int z=0; z<classNamesAttUpd.length();z++) {
								JSONObject attDel = classNamesAttUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeName")) {
							for(int z=0; z<classNamesAttNUpd.length();z++) {
								JSONObject attDel = classNamesAttNUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								classNamesAttNUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassAttributeType")){
							for(int z=0; z<classNamesAttTUpd.length();z++) {
								JSONObject attDel = classNamesAttTUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								classNamesAttTUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperation")) {
							
							for(int z=0; z<classNamesOprUpd.length();z++) {
								JSONObject oprDel = classNamesOprUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationName")) {
							for(int z=0; z<classNamesOprNUpd.length();z++) {
								JSONObject oprDel = classNamesOprNUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprNUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationReturnType")) {
							for(int z=0; z<classNamesOprRTUpd.length();z++) {
								JSONObject oprDel = classNamesOprRTUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								classNamesOprRTUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameter")) {
							for(int z=0; z<classNamesOprParUpd.length();z++) {
								JSONObject oprDel = classNamesOprParUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterName")) {
							for(int z=0; z<classNamesOprParNUpd.length();z++) {
								JSONObject oprDel = classNamesOprParNUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								classNamesOprParNUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("ClassOperationParameterType")) {
							for(int z=0; z<classNamesOprParTUpd.length();z++) {
								JSONObject oprDel = classNamesOprParTUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								classNamesOprParTUpd.put(oprDel);
							}
						}
						else classNamesUpd.put(obj2.getString("ElementName"));
					}
				}
			}
			
			if(obj.has("interfaces")) {
				JSONArray arr = obj.getJSONArray("interfaces");
				for(int j = 0; j<arr.length(); j++) {
					JSONObject obj2 = arr.getJSONObject(j);
					
					if(obj2.getString("OperationType").equals("Delete")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("InterfaceAttribute")) {
							
							for(int z=0; z<intNamesAttDel.length();z++) {
								JSONObject attDel = intNamesAttDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeName")) {
							for(int z=0; z<intNamesAttNDel.length();z++) {
								JSONObject attDel = intNamesAttNDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttNDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeType")){
							for(int z=0; z<intNamesAttTDel.length();z++) {
								JSONObject attDel = intNamesAttTDel.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								intNamesAttTDel.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperation")) {
							
							for(int z=0; z<intNamesOprDel.length();z++) {
								JSONObject oprDel = intNamesOprDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationName")) {
							for(int z=0; z<intNamesOprNDel.length();z++) {
								JSONObject oprDel = intNamesOprNDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprNDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationReturnType")) {
							for(int z=0; z<intNamesOprRTDel.length();z++) {
								JSONObject oprDel = intNamesOprRTDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprRTDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameter")) {
							for(int z=0; z<intNamesOprParDel.length();z++) {
								JSONObject oprDel = intNamesOprParDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterName")) {
							for(int z=0; z<intNamesOprParNDel.length();z++) {
								JSONObject oprDel = intNamesOprParNDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParNDel.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterType")) {
							for(int z=0; z<intNamesOprParTDel.length();z++) {
								JSONObject oprDel = intNamesOprParTDel.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								intNamesOprParTDel.put(oprDel);
							}
						}
						else intNamesDel.put(obj2.getString("ElementName"));
					}
					
					else if(obj2.getString("OperationType").equals("Create")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("InterfaceAttribute")) {
							
							for(int z=0; z<intNamesAttCre.length();z++) {
								JSONObject attDel = intNamesAttCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeName")) {
							for(int z=0; z<intNamesAttNCre.length();z++) {
								JSONObject attDel = intNamesAttNCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttNCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeType")){
							for(int z=0; z<intNamesAttTCre.length();z++) {
								JSONObject attDel = intNamesAttTCre.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								intNamesAttTCre.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperation")) {
							
							for(int z=0; z<intNamesOprCre.length();z++) {
								JSONObject oprDel = intNamesOprCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationName")) {
							for(int z=0; z<intNamesOprNCre.length();z++) {
								JSONObject oprDel = intNamesOprNCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprNCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationReturnType")) {
							for(int z=0; z<intNamesOprRTCre.length();z++) {
								JSONObject oprDel = intNamesOprRTCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprRTCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameter")) {
							for(int z=0; z<intNamesOprParCre.length();z++) {
								JSONObject oprDel = intNamesOprParCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterName")) {
							for(int z=0; z<intNamesOprParNCre.length();z++) {
								JSONObject oprDel = intNamesOprParNCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParNCre.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterType")) {
							for(int z=0; z<intNamesOprParTCre.length();z++) {
								JSONObject oprDel = intNamesOprParTCre.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								intNamesOprParTCre.put(oprDel);
							}
						}
						else intNamesCre.put(obj2.getString("ElementName"));
					}
					else if(obj2.getString("OperationType").equals("Update")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("InterfaceName")) {
							
							intNames.put(obj2.getString("ElementName"));
						}
						
						else if(obj2.getString("ElementType").equals("InterfaceAttribute")) {
							
							for(int z=0; z<intNamesAttUpd.length();z++) {
								JSONObject attDel = intNamesAttUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeName")) {
							for(int z=0; z<intNamesAttNUpd.length();z++) {
								JSONObject attDel = intNamesAttNUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								
								intNamesAttNUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceAttributeType")){
							for(int z=0; z<intNamesAttTUpd.length();z++) {
								JSONObject attDel = intNamesAttTUpd.getJSONObject(z);
								if(attDel.getString("AttributeName").equals(obj2.getString("AttributeName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("AttributeName", obj2.getString("AttributeName"));
								attDel.put("AttributeType", obj2.getString("AttributeType"));
								
								intNamesAttTUpd.put(attDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperation")) {
							
							for(int z=0; z<intNamesOprUpd.length();z++) {
								JSONObject oprDel = intNamesOprUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationName")) {
							for(int z=0; z<intNamesOprNUpd.length();z++) {
								JSONObject oprDel = intNamesOprNUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprNUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationReturnType")) {
							for(int z=0; z<intNamesOprRTUpd.length();z++) {
								JSONObject oprDel = intNamesOprRTUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								
								intNamesOprRTUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameter")) {
							for(int z=0; z<intNamesOprParUpd.length();z++) {
								JSONObject oprDel = intNamesOprParUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterName")) {
							for(int z=0; z<intNamesOprParNUpd.length();z++) {
								JSONObject oprDel = intNamesOprParNUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								
								intNamesOprParNUpd.put(oprDel);
							}
						}
						else if(obj2.getString("ElementType").equals("InterfaceOperationParameterType")) {
							for(int z=0; z<intNamesOprParTUpd.length();z++) {
								JSONObject oprDel = intNamesOprParTUpd.getJSONObject(z);
								if(oprDel.getString("OperationName").equals(obj2.getString("OperationName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject oprDel = new JSONObject();
								oprDel.put("ElementName", obj2.getString("ElementName"));
								oprDel.put("OperationName", obj2.getString("OperationName"));
								oprDel.put("ParameterName", obj2.getString("ParameterName"));
								oprDel.put("ParameterType", obj2.getString("ParameterType"));
								
								intNamesOprParTUpd.put(oprDel);
							}
						}
						else intNamesUpd.put(obj2.getString("ElementName"));
					}
				}
			}
			
			if(obj.has("enums")) {
				JSONArray arr = obj.getJSONArray("enums");
				for(int j = 0; j<arr.length(); j++) {
					JSONObject obj2 = arr.getJSONObject(j);
					
					if(obj2.getString("OperationType").equals("Delete")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("EnumerationLiteral")) {
							
							for(int z=0; z<enumNamesLitDel.length();z++) {
								JSONObject attDel = enumNamesLitDel.getJSONObject(z);
								if(attDel.getString("LiteralName").equals(obj2.getString("LiteralName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attDel = new JSONObject();
								attDel.put("ElementName", obj2.getString("ElementName"));
								attDel.put("LiteralName", obj2.getString("LiteralName"));
								
								enumNamesLitDel.put(attDel);
							}
						}
						
						else enumNamesDel.put(obj2.getString("ElementName"));
					}
					
					else if(obj2.getString("OperationType").equals("Create")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("EnumerationLiteral"))  {
							
							for(int z=0; z<enumNamesLitCre.length();z++) {
								JSONObject attCre = enumNamesLitCre.getJSONObject(z);
								if(attCre.getString("LiteralName").equals(obj2.getString("LiteralName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attCre = new JSONObject();
								attCre.put("ElementName", obj2.getString("ElementName"));
								attCre.put("LiteralName", obj2.getString("LiteralName"));
								
								enumNamesLitCre.put(attCre);
							}
						}
						
						else enumNamesCre.put(obj2.getString("ElementName"));
					}
					else if(obj2.getString("OperationType").equals("Update")) {
						
						boolean encontrado = false;
						
						if(obj2.getString("ElementType").equals("EnumerationName")) {
							
							enumNames.put(obj2.getString("ElementName"));
						}
						
						else if(obj2.getString("ElementType").equals("EnumerationLiteral"))  {
							
							for(int z=0; z<enumNamesLitUpd.length();z++) {
								JSONObject attUpd = enumNamesLitUpd.getJSONObject(z);
								if(attUpd.getString("LiteralName").equals(obj2.getString("LiteralName"))) {
									encontrado = true;
								}
							}
							
							if(!encontrado) {
								JSONObject attUpd = new JSONObject();
								attUpd.put("ElementName", obj2.getString("ElementName"));
								attUpd.put("LiteralName", obj2.getString("LiteralName"));
								
								enumNamesLitUpd.put(attUpd);
							}
						}
						else enumNamesUpd.put(obj2.getString("ElementName"));
					}
				}
			}
			
			if(obj.has("associations")) {
				JSONArray arr = obj.getJSONArray("associations");
				for(int j = 0; j<arr.length(); j++) {
					JSONObject obj2 = arr.getJSONObject(j);
					
					if(obj2.getString("OperationType").equals("Delete")) {
						JSONObject assoDel = new JSONObject();
						//assoDel.put("assoID", "id"+(j+1));
						assoDel.put("SourceName", obj2.getString("SourceName"));
						assoDel.put("TargetName", obj2.getString("TargetName"));
						assoNamesDel.put(assoDel);
					}
					
					else if(obj2.getString("OperationType").equals("Create")) {
						JSONObject assoCre = new JSONObject();
						//assoCre.put("assoID", "id"+(j+1));
						assoCre.put("SourceName", obj2.getString("SourceName"));
						assoCre.put("TargetName", obj2.getString("TargetName"));
						assoNamesCre.put(assoCre);
					}
					
					else if(obj2.getString("OperationType").equals("Update")) {
						JSONObject assoUpd = new JSONObject();
						//assoUpd.put("assoID", "id"+(j+1));
						assoUpd.put("SourceName", obj2.getString("SourceName"));
						assoUpd.put("TargetName", obj2.getString("TargetName"));
						assoUpd.put("UpdateName", obj2.getString("UpdatedName"));
						assoNamesUpd.put(assoUpd);
					}
				}
			}
		}
		
		InputFile = InputFile.replace("\r", "");
		InputFile2 = InputFile2.replace("\r", "");
		
		String[] diagrama = InputFile.split("\n");
		String[] diagrama2 = InputFile2.split("\n");
		JSONArray diagramaAux = new JSONArray();
		
		for(int i = 0; i <diagrama.length; i++) {
			diagramaAux.put(diagrama[i]);
		}
		
		String model = "'";
		
			boolean delete = false, create = false, update = false;
		
			for(int i=0;i<diagrama.length;i++) {
				
				delete = false;
				create = false;
				update = false;
				
					for(int j=0;j<classNamesDel.length();j++) {
				
						if(diagrama[i].contains("class " + classNamesDel.getString(j))) {
					
							model = model + "class " + classNamesDel.getString(j) + ":::delete\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttDel.length();j++) {
						
						JSONObject obj = classNamesAttDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_deleteAll\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttNDel.length();j++) {
						
						JSONObject obj = classNamesAttNDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_delete\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttTDel.length();j++) {
						
						JSONObject obj = classNamesAttTDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						String attributeType = obj.getString("AttributeType");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							int pos = diagrama[i].indexOf("+");
							
							model = model + diagrama[i].substring(0,pos) + "+" + attributeType + "_delete " + attributeName + "\\n";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprDel.length();j++) {
						
						JSONObject obj = classNamesOprDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_deleteAll" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprNDel.length();j++) {
						
						JSONObject obj = classNamesOprNDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_delete" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprRTDel.length();j++) {
						
						JSONObject obj = classNamesOprRTDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							model = model + diagrama[i] + "_delete" + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParDel.length();j++) {
						
						JSONObject obj = classNamesOprParDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos-1) + "_delete " + parameterName + "_delete" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParNDel.length();j++) {
						
						JSONObject obj = classNamesOprParNDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos) + parameterName + "_delete" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParTDel.length();j++) {
						
						JSONObject obj = classNamesOprParTDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						String parameterType = obj.getString("ParameterType");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterType.length()+1;
							
							model = model + diagrama[i].substring(0, pos-lon) + parameterType + "_delete " + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesDel.length();j++) {
						
						if(diagrama[i].contains("class " + intNamesDel.getString(j))) {
					
							model = model + "class " + intNamesDel.getString(j) + ":::delete\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttDel.length();j++) {
						
						JSONObject obj = intNamesAttDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_deleteAll\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttNDel.length();j++) {
						
						JSONObject obj = intNamesAttNDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_delete\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttTDel.length();j++) {
						
						JSONObject obj = intNamesAttTDel.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						String attributeType = obj.getString("AttributeType");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							int pos = diagrama[i].indexOf("+");
							
							model = model + diagrama[i].substring(0,pos) + "+" + attributeType + "_delete " + attributeName + "\\n";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprDel.length();j++) {
						
						JSONObject obj = intNamesOprDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_deleteAll" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprNDel.length();j++) {
						
						JSONObject obj = intNamesOprNDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_delete" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprRTDel.length();j++) {
						
						JSONObject obj = intNamesOprRTDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							model = model + diagrama[i] + "_delete" + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParDel.length();j++) {
						
						JSONObject obj = intNamesOprParDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos-1) + "_delete " + parameterName + "_delete" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParNDel.length();j++) {
						
						JSONObject obj = intNamesOprParNDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos) + parameterName + "_delete" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParTDel.length();j++) {
						
						JSONObject obj = intNamesOprParTDel.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						String parameterType = obj.getString("ParameterType");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterType.length()+1;
							
							model = model + diagrama[i].substring(0, pos-lon) + parameterType + "_delete " + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0; j<classNames.length();j++) {
						
						if(diagrama[i].contains("class " + classNames.getString(j))) {
						
							model = model + "class " + classNames.getString(j) + ":::modificationTitle\\n ";
							update = true;
						}
					}
					
					for(int j=0;j<classNamesUpd.length();j++) {
						
						if(diagrama[i].contains("class " + classNamesUpd.getString(j))) {
					
							model = model + "class " + classNamesUpd.getString(j) + ":::modification\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttUpd.length();j++) {
						
						JSONObject obj = classNamesAttUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_modifyAll\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttNUpd.length();j++) {
						
						JSONObject obj = classNamesAttNUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_modify\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesAttTUpd.length();j++) {
						
						JSONObject obj = classNamesAttTUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						String attributeType = obj.getString("AttributeType");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							int pos = diagrama[i].indexOf("+");
							
							model = model + diagrama[i].substring(0,pos) + "+" + attributeType + "_modify " + attributeName + "\\n";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprUpd.length();j++) {
						
						JSONObject obj = classNamesOprUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_modifyAll" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprNUpd.length();j++) {
						
						JSONObject obj = classNamesOprNUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_modify" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprRTUpd.length();j++) {
						
						JSONObject obj = classNamesOprRTUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							model = model + diagrama[i] + "_modify" + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParUpd.length();j++) {
						
						JSONObject obj = classNamesOprParUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos-1) + "_modify " + parameterName + "_modify" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParNUpd.length();j++) {
						
						JSONObject obj = classNamesOprParNUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos) + parameterName + "_modify" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<classNamesOprParTUpd.length();j++) {
						
						JSONObject obj = classNamesOprParTUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						String parameterType = obj.getString("ParameterType");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterType.length()+1;
							
							model = model + diagrama[i].substring(0, pos-lon) + parameterType + "_modify " + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0; j<intNames.length();j++) {
						
						if(diagrama[i].contains("class " + intNames.getString(j))) {
						
							model = model + "class " + intNames.getString(j) + ":::modificationTitle\\n ";
							update = true;
						}
					}
					
					for(int j=0;j<intNamesUpd.length();j++) {
						
						if(diagrama[i].contains("class " + intNamesUpd.getString(j))) {
					
							model = model + "class " + intNamesUpd.getString(j) + ":::modification\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttUpd.length();j++) {
						
						JSONObject obj = intNamesAttUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_modifyAll\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttNUpd.length();j++) {
						
						JSONObject obj = intNamesAttNUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							model = model + diagrama[i] + "_modify\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesAttTUpd.length();j++) {
						
						JSONObject obj = intNamesAttTUpd.getJSONObject(j);
						
						String attributeName = obj.getString("AttributeName");
						String attributeType = obj.getString("AttributeType");
						
						if(diagrama[i].contains(attributeName) && !diagrama[i].contains("(")) {
							
							int pos = diagrama[i].indexOf("+");
							
							model = model + diagrama[i].substring(0,pos) + "+" + attributeType + "_modify " + attributeName + "\\n";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprUpd.length();j++) {
						
						JSONObject obj = intNamesOprUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_modifyAll" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprNUpd.length();j++) {
						
						JSONObject obj = intNamesOprNUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							String operation = diagrama[i];
							
							int pos = operation.indexOf("(");
							
							model = model + diagrama[i].substring(0,pos) + "_modify" + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprRTUpd.length();j++) {
						
						JSONObject obj = intNamesOprRTUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						
						if(diagrama[i].contains(operationName)) {
							
							model = model + diagrama[i] + "_modify" + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParUpd.length();j++) {
						
						JSONObject obj = intNamesOprParUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos-1) + "_modify " + parameterName + "_modify" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParNUpd.length();j++) {
						
						JSONObject obj = intNamesOprParNUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterName.length();
							
							model = model + diagrama[i].substring(0, pos) + parameterName + "_modify" + diagrama[i].substring(pos+lon) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<intNamesOprParTUpd.length();j++) {
						
						JSONObject obj = intNamesOprParTUpd.getJSONObject(j);
						
						String operationName = obj.getString("OperationName");
						String parameterName = obj.getString("ParameterName");
						String parameterType = obj.getString("ParameterType");
						
						if(diagrama[i].contains(operationName)) {
							
							int pos = diagrama[i].indexOf(parameterName);
							int lon = parameterType.length()+1;
							
							model = model + diagrama[i].substring(0, pos-lon) + parameterType + "_modify " + diagrama[i].substring(pos) + "\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<enumNamesDel.length();j++) {
						
						if(diagrama[i].contains("class " + enumNamesDel.getString(j))) {
					
							model = model + "class " + enumNamesDel.getString(j) + ":::delete{\\n ";
							delete = true;
						}
					}
					
					
					for(int j=0; j<enumNamesUpd.length();j++) {
						
						if(diagrama[i].contains("class " + enumNamesUpd.getString(j))) {
						
							model = model + "class " + enumNamesUpd.getString(j) + ":::modification{\\n ";
							update = true;
						}
					}
					
					for(int j=0; j<enumNames.length();j++) {
						
						if(diagrama[i].contains("class " + enumNames.getString(j))) {
						
							model = model + "class " + enumNames.getString(j) + ":::modificationTitle{\\n ";
							update = true;
						}
					}
					
					for(int j=0;j<enumNamesLitDel.length();j++) {
						
						JSONObject obj = enumNamesLitDel.getJSONObject(j);
						
						String attributeName = obj.getString("LiteralName");
						
						if(diagrama[i].contains(attributeName)) {
							
							model = model + diagrama[i] + "_delete\\n ";
							delete = true;
						}
					}
					
					for(int j=0;j<enumNamesLitUpd.length();j++) {
						
						JSONObject obj = enumNamesLitUpd.getJSONObject(j);
						
						String attributeName = obj.getString("LiteralName");
						
						if(diagrama[i].contains(attributeName)) {
							
							model = model + diagrama[i] + "_modify\\n ";
							delete = true;
						}
					}
					
					for(int j = 0; j<assoNamesDel.length();j++) {
						
						JSONObject obj = assoNamesDel.getJSONObject(j);
						
						if(diagrama[i].contains(obj.getString("SourceName")) && diagrama[i].contains(obj.getString("TargetName"))) {
							
							model = model + diagrama[i] + "x--" + "\\n";
							delete = true;
						}
					}
					
					for(int j = 0; j<assoNamesUpd.length();j++) {
						
						JSONObject obj = assoNamesUpd.getJSONObject(j);
						
						if(diagrama[i].contains(obj.getString("SourceName")) && diagrama[i].contains(obj.getString("TargetName"))) {
							
							model = model + diagrama[i] + "m--" + "\\n";
							delete = true;
						}
					}
				
				if(!delete && !create && !update) {
					
					model = model + diagramaAux.getString(i) + "\\n ";
				}
				
			}
			
			for(int i = 0; i<diagrama2.length; i++) {
				
				for(int j=0;j<classNamesCre.length();j++) {
					
					if(diagrama2[i].contains("class " + classNamesCre.getString(j))) {
				
						model = model + "class " + classNamesCre.getString(j) + ":::create\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesAttCre.length();j++) {
					
					JSONObject obj = classNamesAttCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						model = model + diagrama2[i] + "_createAll\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesAttNCre.length();j++) {
					
					JSONObject obj = classNamesAttNCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						model = model + diagrama2[i] + "_create\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesAttTCre.length();j++) {
					
					JSONObject obj = classNamesAttTCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					String attributeType = obj.getString("AttributeType");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						int pos = diagrama2[i].indexOf("+");
						
						model = model + diagrama2[i].substring(0,pos) + "+" + attributeType + "_create " + attributeName + "\\n";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprCre.length();j++) {
					
					JSONObject obj = classNamesOprCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						String operation = diagrama2[i];
						
						int pos = operation.indexOf("(");
						
						model = model + diagrama2[i].substring(0,pos) + "_createAll" + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprNCre.length();j++) {
					
					JSONObject obj = classNamesOprNCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						String operation = diagrama2[i];
						
						int pos = operation.indexOf("(");
						
						model = model + diagrama2[i].substring(0,pos) + "_create" + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprRTCre.length();j++) {
					
					JSONObject obj = classNamesOprRTCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						model = model + diagrama2[i] + "_create" + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprParCre.length();j++) {
					
					JSONObject obj = classNamesOprParCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterName.length();
						
						model = model + diagrama2[i].substring(0, pos-1) + "_create " + parameterName + "_create" + diagrama2[i].substring(pos+lon) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprParNCre.length();j++) {
					
					JSONObject obj = classNamesOprParNCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterName.length();
						
						model = model + diagrama2[i].substring(0, pos) + parameterName + "_create" + diagrama2[i].substring(pos+lon) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<classNamesOprParTCre.length();j++) {
					
					JSONObject obj = classNamesOprParTCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					String parameterType = obj.getString("ParameterType");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterType.length()+1;
						
						model = model + diagrama2[i].substring(0, pos-lon) + parameterType + "_create " + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesCre.length();j++) {
					
					if(diagrama2[i].contains("class " + intNamesCre.getString(j))) {
				
						model = model + "class " + intNamesCre.getString(j) + ":::create\\n ";
						model = model + "<<interface>>" + intNamesCre.getString(j) + "\\n";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesAttCre.length();j++) {
					
					JSONObject obj = intNamesAttCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						model = model + diagrama2[i] + "_createAll\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesAttNCre.length();j++) {
					
					JSONObject obj = intNamesAttNCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						model = model + diagrama2[i] + "_create\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesAttTCre.length();j++) {
					
					JSONObject obj = intNamesAttTCre.getJSONObject(j);
					
					String attributeName = obj.getString("AttributeName");
					String attributeType = obj.getString("AttributeType");
					
					if(diagrama2[i].contains(attributeName) && !diagrama2[i].contains("(")) {
						
						int pos = diagrama2[i].indexOf("+");
						
						model = model + diagrama2[i].substring(0,pos) + "+" + attributeType + "_create " + attributeName + "\\n";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprCre.length();j++) {
					
					JSONObject obj = intNamesOprCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						String operation = diagrama2[i];
						
						int pos = operation.indexOf("(");
						
						model = model + diagrama2[i].substring(0,pos) + "_createAll" + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprNCre.length();j++) {
					
					JSONObject obj = intNamesOprNCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						String operation = diagrama2[i];
						
						int pos = operation.indexOf("(");
						
						model = model + diagrama2[i].substring(0,pos) + "_create" + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprRTCre.length();j++) {
					
					JSONObject obj = intNamesOprRTCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					
					if(diagrama2[i].contains(operationName)) {
						
						model = model + diagrama2[i] + "_create" + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprParCre.length();j++) {
					
					JSONObject obj = intNamesOprParCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterName.length();
						
						model = model + diagrama2[i].substring(0, pos-1) + "_create " + parameterName + "_create" + diagrama2[i].substring(pos+lon) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprParNCre.length();j++) {
					
					JSONObject obj = intNamesOprParNCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterName.length();
						
						model = model + diagrama2[i].substring(0, pos) + parameterName + "_create" + diagrama2[i].substring(pos+lon) + "\\n ";
						delete = true;
					}
				}
				
				for(int j=0;j<intNamesOprParTCre.length();j++) {
					
					JSONObject obj = intNamesOprParTCre.getJSONObject(j);
					
					String operationName = obj.getString("OperationName");
					String parameterName = obj.getString("ParameterName");
					String parameterType = obj.getString("ParameterType");
					
					if(diagrama2[i].contains(operationName)) {
						
						int pos = diagrama2[i].indexOf(parameterName);
						int lon = parameterType.length()+1;
						
						model = model + diagrama2[i].substring(0, pos-lon) + parameterType + "_create " + diagrama2[i].substring(pos) + "\\n ";
						delete = true;
					}
				}
				
				for(int j = 0; j<assoNamesCre.length();j++) {
					
					JSONObject obj = assoNamesCre.getJSONObject(j);
					
					if(diagrama2[i].contains(obj.getString("SourceName")) && diagrama2[i].contains(obj.getString("TargetName"))) {
						
						model = model + diagrama2[i] + "c--" + "\\n";
						create = true;
					}
				}
				
				for(int j=0; j<enumNamesCre.length();j++) {
					
					if(diagrama2[i].contains("class " + enumNamesCre.getString(j))) {
					
						model = model + "class " + enumNamesCre.getString(j) + ":::create{\\n ";
						create = true;
					}
				}
				
				for(int j=0;j<enumNamesLitCre.length();j++) {
					
					JSONObject obj = enumNamesLitCre.getJSONObject(j);
					
					String attributeName = obj.getString("LiteralName");
					
					if(diagrama2[i].contains(attributeName)) {
						
						model = model + diagrama2[i] + "_create\\n ";
						delete = true;
					}
				}
			}
			
		model = model + ";;;";
		
		for(int i=0;i<diagrama.length;i++) {
			model = model + diagrama[i] + "\\n ";
		}
		
		model = model + ";;;";
		
		for(int i=0;i<diagrama2.length;i++) {
			model = model + diagrama2[i] + "\\n ";
		}
		
		model = model + "'";
		
		return model;
	}

}
