//allDiagrams read the text file that the program ClassModelMermaidWeb give as a result.
//If the diagram want to be changed, put the new content of the text file in the variable allDiagrams
var allDiagrams = 'classDiagram\n class ClassA\n ClassA : +String property1\n ClassA : +getProperty1()\n class ClassB:::modification\n <<interface>> ClassB\n ClassB : +Int_modify Age\nClassB : +Operation_1(String_modify dni) any\n ClassA *-- ClassBx--\n class ClassC:::create\n ClassC: +String Name_createAll\n;;;classDiagram\n class ClassA\n ClassA : +String property1\n ClassA : +getProperty1()\n class ClassB\n <<interface>> ClassB\n ClassB : +String Age\n ClassB : +Operation_1(Int dni) any\n ClassA *-- ClassB\n class ClassD\n class ClassE\n class ClassF\n ClassB -- ClassD\n ClassD -- ClassE;;;classDiagram\n class ClassA\n ClassA : +String property1\n ClassA : +getProperty1()\n class ClassB\n <<interface>> ClassB\n ClassB : +Int Age\n ClassB : +Operation_1(String dni) any\n class ClassC\n ClassC: +String Name\n';
var splitDiagram = allDiagrams.split(";;;");
var diagram1 = splitDiagram[1];
var diagram2 = splitDiagram[2];
var mermaidDiagram = splitDiagram[0];

const idsDel = [];
const idsCre = [];
const idsUpd = [];

var index = 1;
var extraAsso = 0;

var aux = diagram1.split("\n");
for(var i=0;i<aux.length;i++){
    if(aux[i].includes("--")){
        extraAsso++;
    }
}

var aux = diagram2.split("\n");
for(var i=0;i<aux.length;i++){
    if(aux[i].includes("--")){
        extraAsso++;
    }
}

aux = mermaidDiagram.split("\n");

for(var i=0; i<aux.length;i++){

    if(aux[i].includes("x--")){
        idsDel.push(index);
        index++;

        var operation = "Delete";
        var element = "Association";
        var namelement = aux[i].replace("x--","");

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable tbody");
        tableBody.append(markup);
    }

    else if(aux[i].includes("c--")){
        idsCre.push(index);
        index++;

        var operation = "Create";
        var element = "Association";
        var namelement = aux[i].replace("c--","");

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable2 tbody");
        tableBody.append(markup);
    }

    else if(aux[i].includes("m--")){
        idsUpd.push(index);
        index++;

        var operation = "Update";
        var element = "Association";
        var namelement = aux[i].replace("m--","");

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable3 tbody");
        tableBody.append(markup);
    }

    else if(aux[i].includes("--")){
        index++;
    }
}

mermaidDiagram = mermaidDiagram.replaceAll("x--","");
mermaidDiagram = mermaidDiagram.replaceAll("c--","");
mermaidDiagram = mermaidDiagram.replaceAll("m--","");

for(var i=0; i<aux.length;i++){

    if(aux[i].includes("class")){

        var element = "Class";

        if( i+1 < aux.length){
            if(aux[i+1].includes("<<interface>>")){
                element = "Interface";
            }
            else if(aux[i+1].includes("<<enumeration>>")){
                element = "Enumeration";
            }
        }
    }

    if(aux[i].includes(":::delete")){

        var operation = "Delete";

        var namelement = aux[i].replace(":::delete","");
        namelement = namelement.substring(7);

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable tbody");
        tableBody.append(markup);

    }

    if(aux[i].includes("_delete")){

        if(aux[i].includes("(")){
            element = element + " Operation";

            var aux2 = aux[i].split("(");

            if(aux2[1].includes("_delete")){

                element = element + " Parameter";
            }

            var aux3 = aux[i].split(")");
            console.log(aux3);

            if(aux3[1].includes("_delete")){

                element = element + " Return Type";
                
            }
        }
        else{
            element = element + " Attribute";
        }

        var operation = "Delete";

        var namelement = aux[i].replace("_deleteAll","");
        namelement = namelement.replace("_delete","");

        var aux2 = namelement.split(":");
        namelement = aux2[1].substring(2);

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable tbody");
        tableBody.append(markup);

        element = element.replace("Operation","");
        element = element.replace("Attribute","");
        
    }

    if(aux[i].includes(":::create")){

        var operation = "Create";

        var namelement = aux[i].replace(":::create","");
        namelement = namelement.substring(7);

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable2 tbody");
        tableBody.append(markup);

    }

    if(aux[i].includes("_create")){

        if(aux[i].includes("(")){
            element = element + " Operation";

            var aux2 = aux[i].split("(");

            if(aux2[1].includes("_create")){

                element = element + " Parameter";
            }

            var aux3 = aux[i].split(")");
            console.log(aux3);

            if(aux3[1].includes("_create")){

                element = element + " Return Type";
                
            }
        }
        else{
            element = element + " Attribute";
        }

        var operation = "Create";

        var namelement = aux[i].replace("_createAll","");
        namelement = namelement.replace("_create","");

        var aux2 = namelement.split(":");
        namelement = aux2[1].substring(2);

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable2 tbody");
        tableBody.append(markup);

        element = element.replace("Operation","");
        element = element.replace("Attribute","");
        
    }

    if(aux[i].includes("_modify")){

        if(aux[i].includes("(")){
            element = element + " Operation";

            var aux2 = aux[i].split("(");

            if(aux2[1].includes("_modify")){

                element = element + " Parameter";
            }

            var aux3 = aux[i].split(")");
            console.log(aux3);

            if(aux3[1].includes("_modify")){

                element = element + " Return Type";
                
            }
        }
        else{
            element = element + " Attribute";
        }

        var operation = "Update";

        var namelement = aux[i].replace("_modifyAll","");
        namelement = namelement.replace("_modify","");

        var aux2 = namelement.split(":");
        namelement = aux2[1].substring(2);

        var markup = "<tr><td>"+operation+"</td><td>"+element+"</td><td>"+namelement+"</td></tr>";
        var tableBody = $("#myTable3 tbody");
        tableBody.append(markup);

        element = element.replace("Operation","");
        element = element.replace("Attribute","");
        
    }
}

var diagram = diagram1;

el = document.getElementById("myDiagram");
el.innerHTML = "";
el.textContent = diagram;

el = document.getElementById("diagram2");
el.innerHTML = "";
el.textContent = diagram2;

mermaid.init();