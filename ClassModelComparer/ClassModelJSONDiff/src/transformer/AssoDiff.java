package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class AssoDiff {
	
	public JSONArray getAssoDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrAsso = new JSONArray();
		if(obj.has("associations"))	arrAsso = obj.getJSONArray("associations");
		
		//Process associations
		
		for(int k = 0; k < arrAsso.length(); k++) {
			
			//Search for deleted associations
			
			JSONObject objAsso = arrAsso.getJSONObject(k);
			
			JSONObject source = objAsso.getJSONObject("source");
			JSONObject target = objAsso.getJSONObject("target");
			
			String sourceID = source.getString("id");
			String targetID = target.getString("id");
			String sourceName = "";
			if(source.has("name"))sourceName = source.getString("name");
			String targetName = "";
			if(target.has("name"))targetName = target.getString("name");
			String updatedSource = "";
			String updatedTarget = "";
			Boolean sourceNameB = false;
			Boolean targetNameB = false;
			
			encontrado = false;
			
			//Find the association in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objAsso2 =  fileDiff.getJSONObject(k2);
				
				if(!objAsso2.has("source")) break;
				JSONObject source2 = objAsso2.getJSONObject("source");
				JSONObject target2 = objAsso2.getJSONObject("target");
				
				if(sourceID.equals(source2.getString("id")) && targetID.equals(target2.getString("id"))) {
					encontrado = true;
					
					if(source.has("name") && sourceName.equals(source2.getString("name"))) {
						sourceNameB = true;
					} else if (source.has("name") && !sourceName.equals(source2.getString("name"))) {
						updatedSource = source2.getString("name");
					}
					if(target.has("name") && targetName.equals(target2.getString("name"))) {
						targetNameB = true;
					} else if(target.has("name") && !targetName.equals(target2.getString("name"))) {
						updatedTarget = target2.getString("name");
					}
					
					if(!source.has("name")) {
						sourceNameB = true;
					}
					if(!target.has("name")) {
						targetNameB = true;
					}
				}
			}
			
			//If the association is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Association");
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
				sourceNameB = true;
				targetNameB = true;
			}
			if(sourceNameB == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","AssociationSource");
				diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("UpdatedName", updatedSource);
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			if(targetNameB == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","AssociationTarget");
				diferencia.put("UpdatedName", updatedTarget);
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created associations
			
			JSONObject objAsso = fileDiff.getJSONObject(k);
			
			JSONObject source = objAsso.getJSONObject("source");
			JSONObject target = objAsso.getJSONObject("target");
			
			String sourceID = source.getString("id");
			String targetID = target.getString("id");
			
			encontrado = false;
			
			//Find the association in the first file
			
			for(int k2 = 0; k2 < arrAsso.length(); k2++) {
				
				JSONObject objAsso2 =  arrAsso.getJSONObject(k2);
				
				if(!objAsso2.has("source")) break;
				JSONObject source2 = objAsso2.getJSONObject("source");
				JSONObject target2 = objAsso2.getJSONObject("target");
				
				if(sourceID.equals(source2.getString("id")) && targetID.equals(target2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the association is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Association");
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			
		}
		
		return diferencias;
	}
	
	public JSONArray getAssoDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrAsso = new JSONArray();
		if(obj.has("associations"))	arrAsso = obj.getJSONArray("associations");
		
		//Process associations
		
		for(int k = 0; k < arrAsso.length(); k++) {
			
			//Search for deleted associations
			
			JSONObject objAsso = arrAsso.getJSONObject(k);
			
			JSONObject source = objAsso.getJSONObject("source");
			JSONObject target = objAsso.getJSONObject("target");
			
			String sourceID = source.getString("id");
			String targetID = target.getString("id");
			String sourceName = "";
			if(source.has("name"))sourceName = source.getString("name");
			String targetName = "";
			if(target.has("name"))targetName = target.getString("name");
			String updatedSource = "";
			String updatedTarget = "";
			Boolean sourceNameB = false;
			Boolean targetNameB = false;
			
			encontrado = false;
			
			//Find the association in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objAsso2 =  fileDiff.getJSONObject(k2);
				
				if(!objAsso2.has("source")) break;
				JSONObject source2 = objAsso2.getJSONObject("source");
				JSONObject target2 = objAsso2.getJSONObject("target");
				
				if(sourceID.equals(source2.getString("id")) && targetID.equals(target2.getString("id"))) {
					encontrado = true;
					
					if(source.has("name") && sourceName.equals(source2.getString("name"))) {
						sourceNameB = true;
					} else if (source.has("name") && !sourceName.equals(source2.getString("name"))) {
						updatedSource = source2.getString("name");
					}
					if(target.has("name") && targetName.equals(target2.getString("name"))) {
						targetNameB = true;
					} else if(target.has("name") && !targetName.equals(target2.getString("name"))) {
						updatedTarget = target2.getString("name");
					}
					
					if(!source.has("name")) {
						sourceNameB = true;
					}
					if(!target.has("name")) {
						targetNameB = true;
					}
				}
			}
			
			//If the association is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_Association");
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
				sourceNameB = true;
				targetNameB = true;
			}
			if(sourceNameB == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_AssociationSource");
				diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("UpdatedName", updatedSource);
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			if(targetNameB == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_AssociationTarget");
				diferencia.put("UpdatedName", updatedTarget);
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created associations
			
			JSONObject objAsso = fileDiff.getJSONObject(k);
			
			JSONObject source = objAsso.getJSONObject("source");
			JSONObject target = objAsso.getJSONObject("target");
			
			String sourceID = source.getString("id");
			String targetID = target.getString("id");
			
			encontrado = false;
			
			//Find the association in the first file
			
			for(int k2 = 0; k2 < arrAsso.length(); k2++) {
				
				JSONObject objAsso2 =  arrAsso.getJSONObject(k2);
				
				if(!objAsso2.has("source")) break;
				JSONObject source2 = objAsso2.getJSONObject("source");
				JSONObject target2 = objAsso2.getJSONObject("target");
				
				if(sourceID.equals(source2.getString("id")) && targetID.equals(target2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the association is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_Association");
				if(source.has("name")) diferencia.put("SourceName", source.getString("name"));
				if(target.has("name")) diferencia.put("TargetName", target.getString("name"));
				diferencia.put("ElementID", sourceID);
				diferencias.put(diferencia);
			}
			
		}
		
		return diferencias;
	}

}
