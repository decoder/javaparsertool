package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class PackDiff {
	
	public JSONArray getPackDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrPack = new JSONArray();
		if(obj.has("packages"))	arrPack = obj.getJSONArray("packages");
		
		DataDiff dataDiff = new DataDiff();
		InstanceDiff instanceDiff = new InstanceDiff();
		EnumsDiff enumsDiff = new EnumsDiff();
		ClasesDiff clasesDiff = new ClasesDiff();
		InterfacesDiff interfacesDiff = new InterfacesDiff();
		AssoDiff assoDiff = new AssoDiff();
		UsageDiff usageDiff = new UsageDiff();
		DepDiff depDiff = new DepDiff();
		PrimDiff primDiff = new PrimDiff();
		
		//Process packages
		
		for(int k = 0; k < arrPack.length(); k++) {
			
			//Search for deleted packages
			
			JSONObject objPack = arrPack.getJSONObject(k);
			
			String id = objPack.getString("id");
			String name = objPack.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find package in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objPack2 =  fileDiff.getJSONObject(k2);

				if(id.equals(objPack2.getString("id"))) {
					
					encontrado = true;
					
					if(name.equals(objPack2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the package is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPack.getString("name"));
				diferencias.put(diferencia);
			} else if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","PackageName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPack.getString("name"));
				diferencias.put(diferencia);
			}
			
			//If the package is found
			
			else if(encontrado == true) {
				
				if(objPack.has("packages")) {
					JSONArray paquetesDiff = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("packages");
					
					paquetesDiff = getPackDiff(objPack,arrDiff);
					
					
					for(int i = 0; i<paquetesDiff.length();i++) {
						diferencias.put(paquetesDiff.getJSONObject(i));
					}
				}
				
				if(objPack.has("dataTypes")) {
					JSONArray dataTypesDiff = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("dataTypes");
					
					dataTypesDiff = dataDiff.getDataDiff(objPack,arrDiff,true);
					
					for(int i=0; i<dataTypesDiff.length();i++) {
						diferencias.put(dataTypesDiff.getJSONObject(i));
					}
				}
				
				if(objPack.has("instanceSpecification")) {
					JSONArray instanceDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("instanceSpecification");
					
					instanceDiffArr = instanceDiff.getInstanceDiff(objPack,arrDiff,true);
					
					for(int i=0; i<instanceDiffArr.length();i++) {
						diferencias.put(instanceDiffArr.getJSONObject(i));
					}	
				}
				
				if(objPack.has("enums")) {
					JSONArray enumsDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("enums");
					
					enumsDiffArr = enumsDiff.getEnumsDiff(objPack,arrDiff,true);
					
					for(int i=0; i<enumsDiffArr.length();i++) {
						diferencias.put(enumsDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("classes")) {
					JSONArray clasesDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("classes");
					
					clasesDiffArr = clasesDiff.getClasesDiff(objPack,arrDiff,true);
					
					for(int i=0; i<clasesDiffArr.length();i++) {
						diferencias.put(clasesDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("interfaces")) {
					JSONArray interfacesDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("interfaces");
					
					interfacesDiffArr = interfacesDiff.getInterfacesDiff(objPack,arrDiff,true);
					
					for(int i=0;i<interfacesDiffArr.length();i++) {
						diferencias.put(interfacesDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("associations")) {
					JSONArray assoDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("associations");
					
					assoDiffArr = assoDiff.getAssoDiff(objPack,arrDiff,true);
					
					for(int i=0;i<assoDiffArr.length();i++) {
						diferencias.put(assoDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("usage")) {
					JSONArray usageDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("usage");
					
					usageDiffArr = usageDiff.getUsageDiff(objPack,arrDiff,true);
					
					for(int i=0;i<usageDiffArr.length();i++) {
						diferencias.put(usageDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("dependency")) {
					JSONArray depDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("dependency");
					
					depDiffArr = depDiff.getDepDiff(objPack,arrDiff,true);
					
					for(int i=0;i<depDiffArr.length();i++) {
						diferencias.put(depDiffArr.getJSONObject(i));
					}
				}
				
				if(objPack.has("primitiveType")) {
					JSONArray primDiffArr = new JSONArray();
					
					JSONObject objPack2 = new JSONObject();
					objPack2 = fileDiff.getJSONObject(k);
					JSONArray arrDiff = new JSONArray();
					arrDiff = objPack2.getJSONArray("primitiveType");
					
					primDiffArr = primDiff.getPrimDiff(objPack,arrDiff,true);
					
					for(int i=0;i<primDiffArr.length();i++) {
						diferencias.put(primDiffArr.getJSONObject(i));
					}
				}
				
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created packages
			
			JSONObject objPack = fileDiff.getJSONObject(k);
			
			String id = objPack.getString("id");
			encontrado = false;
			
			//Find package in the first file
			
			for(int k2 = 0; k2 < arrPack.length(); k2++) {
				
				JSONObject objPack2 =  arrPack.getJSONObject(k2);

				if(id.equals(objPack2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the package is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPack.getString("name"));
				diferencias.put(diferencia);
			}

		}
		
		
		return diferencias;
	}

}
