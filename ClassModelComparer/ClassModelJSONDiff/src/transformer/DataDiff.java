package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class DataDiff {
	
	public JSONArray getDataDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrData = new JSONArray();
		
		if(obj.has("dataType")) arrData = obj.getJSONArray("dataType");
		else if(obj.has("dataTypes")) arrData = obj.getJSONArray("dataTypes");
		
		//Process dataTypes
		
		for(int k = 0; k < arrData.length(); k++) {
			
			//Search for deleted dataTypes
			
			JSONObject objData = arrData.getJSONObject(k);
			
			String id = objData.getString("id");
			String name = objData.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find dataType in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objPrim2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
					
					if(name.equals(objPrim2.getString("name"))) {
						
						modificado = true;
						
					}
				}
			}
			
			//Is the dataType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","DataType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			
			else if(modificado == false){
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","DataTypeName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
			}
			
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created dataTypes
			
			JSONObject objData = fileDiff.getJSONObject(k);
			
			String id = objData.getString("id");
			encontrado = false;
			
			//Find dataType in the first file
			
			for(int k2 = 0; k2 < arrData.length(); k2++) {
				
				JSONObject objPrim2 =  arrData.getJSONObject(k2);
				
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dataType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","DataType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		return diferencias;
	}
	
	public JSONArray getDataDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrData = new JSONArray();
		
		if(obj.has("dataType")) arrData = obj.getJSONArray("dataType");
		else if(obj.has("dataTypes")) arrData = obj.getJSONArray("dataTypes");
		
		//Process dataTypes
		
		for(int k = 0; k < arrData.length(); k++) {
			
			//Search for deleted dataTypes
			
			JSONObject objData = arrData.getJSONObject(k);
			
			String id = objData.getString("id");
			String name = objData.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find dataType in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objPrim2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
					
					if(name.equals(objPrim2.getString("name"))) {
						
						modificado = true;
					}
				}
			}
			
			//If the dataType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_DataType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			
			else if(modificado == false){
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_DataTypeName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
			}
			
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created dataTypes
			
			JSONObject objData = fileDiff.getJSONObject(k);
			
			String id = objData.getString("id");
			encontrado = false;
			
			//Find the dataType in the first file
			
			for(int k2 = 0; k2 < arrData.length(); k2++) {
				
				JSONObject objPrim2 =  arrData.getJSONObject(k2);
				
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dataType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_DataType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objData.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		return diferencias;
	}

}
