package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class InterfacesDiff {
	
	public JSONArray getInterfacesDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrInterfaces = new JSONArray();
				
		if(obj.has("interfaces")) arrInterfaces = obj.getJSONArray("interfaces");
		
		//Process interfaces
		
		for(int k = 0; k < arrInterfaces.length(); k++) {
			
			//Search for deleted interfaces
			
			JSONObject objInterface = arrInterfaces.getJSONObject(k);
			
			String id = objInterface.getString("id");
			String name = objInterface.getString("name");
			String interfaceName = objInterface.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find interface in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objInterface2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objInterface2.getString("id"))) {
					
					encontrado = true;
					
					if(name.equals(objInterface2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the interface is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Interface");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
				modificado = true;
			}
			else if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","InterfaceName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
			}
			
			//If the interface is found, check the rest of the elements
			
			else {
				if(objInterface.has("attributes")) {
					
					JSONArray atributos = objInterface.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type"))atrType = atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrTypeB = false;
						Boolean atrUpdatedB = false;
						Boolean atrUpdatedTypeB = false;
						
						//Find attributes in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objInterface2 =  fileDiff.getJSONObject(k2);
							
							if(objInterface2.has("attributes")) {
							
							JSONArray atributos2 = objInterface2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								//Check attribute name
								
								if(atrID.equals(atributo2.getString("id"))) {
									atrIDB = true;
									
									if(atributo.has("name")) {
										if(atributo2.has("name")) {
											atrNameB = true;
											
											if(atrName.equals(atributo2.getString("name"))) {
												atrUpdatedB = true;
											}
										}
									}
									
									if(atributo.has("type")) {
										if(atributo2.has("type")) {
											atrTypeB = true;
											
											if(atrType.equals(atributo2.getString("type"))) {
												atrUpdatedTypeB = true;
											}
										}
									}
								}
								
								
							}
							}
	
							}
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
							atrUpdatedB = true;
							atrUpdatedTypeB = true;
							
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrUpdatedB = true;
						}
						if(atrTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrUpdatedTypeB = true;
						}
						if(atrUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(atrUpdatedTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
					}
				}
				
				//Check operations
				
				if(objInterface.has("operations")) {
					
					JSONArray operaciones = objInterface.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String oprID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						Boolean oprIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parIDB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						Boolean oprUpdatedB = false;
						Boolean oprUpdatedTypeB = false;
						Boolean parUpdatedB = false;
						Boolean parUpdatedTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objInterfaces2 =  fileDiff.getJSONObject(k2);
							
							if(objInterfaces2.has("operations")) {
							
							JSONArray operaciones2 = objInterfaces2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(oprID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion.has("name")) {
										if(operacion2.has("name")) {
											oprNameB = true;
											
											if(oprName.equals(operacion2.getString("name"))) {
												oprUpdatedB = true;
											}
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
											
											if(oprReturnType.equals(operacion2.getString("returnType"))) {
												oprUpdatedTypeB = true;
											}
										}
									}
								}
								
								//Check operation name
								
								if(oprID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									
									if(parametros.length()>0) {
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parNameB = false;
											
											if(!operacion2.has("parameters")) {
												parIDB = false;
											}else {
												
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int j = 0; j < parametros2.length(); j++) {
													
													JSONObject parametro2 = parametros2.getJSONObject(j);
													
													if(parID.equals(parametro2.getString("id"))) {
														parIDB = true;
																												
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
																
																if(parName.equals(parametro2.getString("name"))) {
																	parUpdatedB = true;
																}
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
																
																if(parType.equals(parametro2.getString("type"))) {
																	parUpdatedTypeB = true;
																}
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","InterfaceOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
												parUpdatedB = true;
												parUpdatedTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parUpdatedB = true;
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parUpdatedTypeB = true;
											}
											if(parUpdatedB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
											if(parUpdatedTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
							oprUpdatedB = true;
							oprUpdatedTypeB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprUpdatedB = true;
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprUpdatedTypeB = true;
						}
						if(oprUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(oprUpdatedTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
						
					}
				}
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created interfaces
			
			JSONObject objInterface = fileDiff.getJSONObject(k);
			
			String id = objInterface.getString("id");
			String interfaceName = objInterface.getString("name");
			encontrado = false;
			
			//Find interface in the first file
			
			for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
				
				JSONObject objInterface2 =  arrInterfaces.getJSONObject(k2);
				
				if(id.equals(objInterface2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the interface is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Interface");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
			}
			
			//If the interface is found, check the rest of the elements
			
			else {
				if(objInterface.has("attributes")) {
					
					JSONArray atributos = objInterface.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type"))atrType = atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrTypeB = false;
						
						//Find attributes in the first file
						
						for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
							
							JSONObject objInterface2 =  arrInterfaces.getJSONObject(k2);
							
							if(objInterface2.has("attributes") && id.equals(objInterface2.getString("id"))) {
							
							JSONArray atributos2 = objInterface2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								//Check attribute name
								
								if(atrID.equals(atributo2.getString("id"))) {
									atrIDB = true;
									
									if(atributo.has("name")) {
										if(atributo2.has("name")) {
											atrNameB = true;
										}
									}
									
									if(atributo.has("type")) {
										if(atributo2.has("type")) {
											atrTypeB = true;
										}
									}
								}
							}
							}
	
							}
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(atrTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
					}
				}
				
				//Check operations
				
				if(objInterface.has("operations")) {
					
					JSONArray operaciones = objInterface.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String oprID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						Boolean oprIDB = false;
						Boolean parIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
							
							JSONObject objInterfaces2 =  arrInterfaces.getJSONObject(k2);
							
							if(objInterfaces2.has("operations") && id.equals(objInterfaces2.getString("id"))) {
							
							JSONArray operaciones2 = objInterfaces2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(oprID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion.has("name")) {
										if(operacion2.has("name")) {
											oprNameB = true;
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
										}
									}
								}
								
								//Check operation name
								
								if(oprID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									
									if(parametros.length()>0) {
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parNameB = false;
											
											if(!operacion2.has("parameters") || operacion2.getJSONArray("parameters").length()==0) {
												parIDB = false;
											}
											else {
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int t = 0; t < parametros2.length(); t++) {
													JSONObject parametro2 = parametros2.getJSONObject(t);
													
													if(parametro.getString("id").equals(parametro2.getString("id"))) {
														parIDB = true;
														
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","InterfaceOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
						
					}
				}
			}
		}
		
		return diferencias;
	}
	
	public JSONArray getInterfacesDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrInterfaces = new JSONArray();
				
		if(obj.has("interfaces")) arrInterfaces = obj.getJSONArray("interfaces");
		
		//Process interfaces
		
		for(int k = 0; k < arrInterfaces.length(); k++) {
			
			//Search for deleted interfaces
			
			JSONObject objInterface = arrInterfaces.getJSONObject(k);
			
			String id = objInterface.getString("id");
			String name = objInterface.getString("name");
			String interfaceName = objInterface.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find interface in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objInterface2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objInterface2.getString("id"))) {
					
					encontrado = true;
					
					if(name.equals(objInterface2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the interface is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_Interface");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
				modificado = true;
			}
			else if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_InterfaceName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
			}
			
			//If the interface is found, check the rest of the elements
			
			else {
				if(objInterface.has("attributes")) {
					
					JSONArray atributos = objInterface.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type"))atrType = atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrTypeB = false;
						Boolean atrUpdatedB = false;
						Boolean atrUpdatedTypeB = false;
						
						//Find attributes in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objInterface2 =  fileDiff.getJSONObject(k2);
							
							if(objInterface2.has("attributes")) {
							
							JSONArray atributos2 = objInterface2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								//Check attribute name
								
								if(atrID.equals(atributo2.getString("id"))) {
									atrIDB = true;
									
									if(atributo.has("name")) {
										if(atributo2.has("name")) {
											atrNameB = true;
											
											if(atrName.equals(atributo2.getString("name"))) {
												atrUpdatedB = true;
											}
										}
									}
									
									if(atributo.has("type")) {
										if(atributo2.has("type")) {
											atrTypeB = true;
											
											if(atrType.equals(atributo2.getString("type"))) {
												atrUpdatedTypeB = true;
											}
										}
									}
								}
								
								
							}
							}
	
							}
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
							atrUpdatedB = true;
							atrUpdatedTypeB = true;
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrUpdatedB = true;
						}
						if(atrTypeB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrUpdatedTypeB = true;
						}
						if(atrUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(atrUpdatedTypeB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
					}
				}
				
				//Check operations
				
				if(objInterface.has("operations")) {
					
					JSONArray operaciones = objInterface.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String oprID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						Boolean oprIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parIDB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						Boolean oprUpdatedB = false;
						Boolean oprUpdatedTypeB = false;
						Boolean parUpdatedB = false;
						Boolean parUpdatedTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objInterfaces2 =  fileDiff.getJSONObject(k2);
							
							if(objInterfaces2.has("operations")) {
							
							JSONArray operaciones2 = objInterfaces2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(oprID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion.has("name")) {
										if(operacion2.has("name")) {
											oprNameB = true;
											
											if(oprName.equals(operacion2.getString("name"))) {
												oprUpdatedB = true;
											}
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
											
											if(oprReturnType.equals(operacion2.getString("returnType"))) {
												oprUpdatedTypeB = true;
											}
										}
									}
								}
								
								//Check operation name
								
								if(oprID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									
									if(parametros.length()>0) {
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parNameB = false;
											
											if(!operacion2.has("parameters")) {
												parIDB = false;
											}else {
												
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int j = 0; j < parametros2.length(); j++) {
													
													JSONObject parametro2 = parametros2.getJSONObject(j);
													
													if(parID.equals(parametro2.getString("id"))) {
														parIDB = true;
																												
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
																
																if(parName.equals(parametro2.getString("name"))) {
																	parUpdatedB = true;
																}
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
																
																if(parType.equals(parametro2.getString("type"))) {
																	parUpdatedTypeB = true;
																}
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_InterfaceOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
												parUpdatedB = true;
												parUpdatedTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parUpdatedB = true;
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parUpdatedTypeB = true;
											}
											if(parUpdatedB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","Package_InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
											if(parUpdatedTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","Package_InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
							oprUpdatedB = true;
							oprUpdatedTypeB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprUpdatedB = true;
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprUpdatedTypeB = true;
						}
						if(oprUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(oprUpdatedTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
						
					}
				}
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created interfaces
			
			JSONObject objInterface = fileDiff.getJSONObject(k);
			
			String id = objInterface.getString("id");
			String interfaceName = objInterface.getString("name");
			encontrado = false;
			
			//Find interface in the first file
			
			for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
				
				JSONObject objInterface2 =  arrInterfaces.getJSONObject(k2);
				
				if(id.equals(objInterface2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the interface is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_Interface");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", interfaceName);
				diferencias.put(diferencia);
			}
			
			//If the interface is found, check the rest of the elements
			
			else {
				if(objInterface.has("attributes")) {
					
					JSONArray atributos = objInterface.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type"))atrType = atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrTypeB = false;
						
						//Find attributes in the first file
						
						for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
							
							JSONObject objInterface2 =  arrInterfaces.getJSONObject(k2);
							
							if(objInterface2.has("attributes") && id.equals(objInterface2.getString("id"))) {
							
							JSONArray atributos2 = objInterface2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								//Check attribute name
								
								if(atrID.equals(atributo2.getString("id"))) {
									atrIDB = true;
									
									if(atributo.has("name")) {
										if(atributo2.has("name")) {
											atrNameB = true;
										}
									}
									
									if(atributo.has("type")) {
										if(atributo2.has("type")) {
											atrTypeB = true;
										}
									}
								}
							}
							}
	
							}
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(atrTypeB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						
					}
				}
				
				//Check operations
				
				if(objInterface.has("operations")) {
					
					JSONArray operaciones = objInterface.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String oprID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						Boolean oprIDB = false;
						Boolean parIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < arrInterfaces.length(); k2++) {
							
							JSONObject objInterfaces2 =  arrInterfaces.getJSONObject(k2);
							
							if(objInterfaces2.has("operations") && id.equals(objInterfaces2.getString("id"))) {
							
							JSONArray operaciones2 = objInterfaces2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(oprID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion.has("name")) {
										if(operacion2.has("name")) {
											oprNameB = true;
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
										}
									}
								}
								
								//Check operation name
								
								if(oprID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									if(parametros.length()>0) {
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parNameB = false;
											
											if(!operacion2.has("parameters") || operacion2.getJSONArray("parameters").length()==0) {
												parIDB = false;
											}
											else {
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int t = 0; t < parametros2.length(); t++) {
													JSONObject parametro2 = parametros2.getJSONObject(t);
													
													if(parametro.getString("id").equals(parametro2.getString("id"))) {
														parIDB = true;
														
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","Package_InterfaceOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","Package_InterfaceOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Create");
												diferencia.put("ElementType","Package_InterfaceOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementName", interfaceName);
												diferencia.put("ElementID", id);
												diferencias.put(diferencia);
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_InterfaceOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementName", interfaceName);
							diferencia.put("ElementID", id);
							diferencias.put(diferencia);
						}	
					}
				}
			}
		}
		
		return diferencias;
	}

}
