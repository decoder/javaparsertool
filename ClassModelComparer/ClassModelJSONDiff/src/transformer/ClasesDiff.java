package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class ClasesDiff {
	
	public JSONArray getClasesDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrClasses = new JSONArray();
		if(obj.has("classes"))	arrClasses = obj.getJSONArray("classes");
		
		//Process classes
		
		for(int k = 0; k < arrClasses.length(); k++) {
			
			//Search for deleted classes
			
			JSONObject objClasses = arrClasses.getJSONObject(k);
			
			String id = objClasses.getString("id");
			String className = objClasses.getString("name");
			Boolean modificadoName = false;
			encontrado = false;
			
			//Find the class in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					
					//If the class has been found
					
					encontrado = true;
					
					//Check the class name
					
					if(className.equals(objClasses2.getString("name"))) {
						modificadoName = true;
					}
				}
			}
			
			//If the class is not found
			
			
			//If the class is an association-class
			
			if(objClasses.has("source") && encontrado==false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType", "AssociationClass");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}else if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType", "Class");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}
			else if(objClasses.has("source") && modificadoName==false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType", "AssociationClassName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}else if(modificadoName == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType", "ClassName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}
			
			//If the class has been found, check the rest of elements
			
			else {
				
				//Check associations
				
				if(objClasses.has("source") && objClasses.has("target")) {
					
					JSONObject source = objClasses.getJSONObject("source");
					JSONObject target = objClasses.getJSONObject("target");
					
					String sourceID = source.getString("id");
					String sourceName = source.getString("name");
					String targetID = target.getString("id");
					String targetName = target.getString("name");
					Boolean modificadoSource = false;
					Boolean modificadoTarget = false;
					
					for(int i = 0; i < fileDiff.length(); i++) {
						
						JSONObject objClasses2 = fileDiff.getJSONObject(i);
						modificadoSource = false;
						modificadoTarget = false;
						
						if(objClasses2.has("source") && objClasses2.has("target")) {
							
							JSONObject source2 = objClasses2.getJSONObject("source");
							JSONObject target2 = objClasses2.getJSONObject("target");
							
							if(sourceID.equals(source2.getString("id")) && sourceName.equals(source2.getString("name"))) {
								
								modificadoSource = true;
								
							}
							
							if(targetID.equals(target2.getString("id")) && targetName.equals(target2.getString("name"))) {
								
								modificadoTarget = true;
								
							}
						}
					}
					
					if(modificadoSource == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType", "AssociationClassSource");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", className);
						diferencias.put(diferencia);
					}
					if(modificadoTarget == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType", "AssociationClassTarget");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", className);
						diferencias.put(diferencia);
					}
				}
				
				if(objClasses.has("attributes")) {
					
					JSONArray atributos = objClasses.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type")) atrType=atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrUpdatedB = false;
						Boolean atrTypeB = false;
						Boolean atrUpdatedTypeB = false;
						
						//Find attributes in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("attributes") && id.equals(objClasses2.getString("id"))) {	
							
							JSONArray atributos2 = objClasses2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								if(atrID.equals(atributo2.getString("id"))) {
									
									atrIDB = true;
									
									if(atributo2.has("name")){
										
										atrNameB = true;
									
										if(atrName.equals(atributo2.getString("name"))) {
											
											atrUpdatedB = true;
											
										}
											
										if(atributo2.has("type")) {
												
											atrTypeB = true;
										
											if(atrType.equals(atributo2.getString("type"))) {
													atrUpdatedTypeB = true;
												}
											}
									}
								}
							}}}
						
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
							atrUpdatedB = true;
							atrUpdatedTypeB = true;
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrTypeB = true;
							atrUpdatedB = true;
						}
						if(atrTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrUpdatedTypeB = true;
						}
						if(atrUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(atrUpdatedTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrTypeB = true;
						}
						
					}
				}
				
				//Check operations
				
				if(objClasses.has("operations") && !objClasses.getJSONArray("operations").isEmpty()) {
					
					JSONArray operaciones = objClasses.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String opID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						
						Boolean oprIDB = false;
						Boolean parIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						Boolean oprUpdatedB = false;
						Boolean oprUpdatedRTB = false;
						Boolean parUpdatedB = false;
						Boolean parUpdatedTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("operations") && id.equals(objClasses2.getString("id"))) {
								
							JSONArray operaciones2 = objClasses2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(opID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion2.has("name")) {
										
										oprNameB = true;
										
										if(oprName.equals(operacion2.getString("name"))) {
											oprUpdatedB = true;
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
											
											if(oprReturnType.equals(operacion2.getString("returnType"))) {
												oprUpdatedRTB = true;
											}
										}
									}					
								}
								
								//Check operation parameters
								
								if(opID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									
									if(!parametros.isEmpty()) {
										
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parIDB = false;
											parNameB = false;
											
											if(!operacion2.has("parameters")) {
												parIDB = false;
											}else {
												
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int j = 0; j < parametros2.length(); j++) {
													
													JSONObject parametro2 = parametros2.getJSONObject(j);
													
													if(parID.equals(parametro2.getString("id"))) {
														parIDB = true;
														
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
																
																if(parName.equals(parametro2.getString("name"))) {
																	parUpdatedB = true;
																}
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
																
																if(parType.equals(parametro2.getString("type"))) {
																	parUpdatedTypeB = true;
																}
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","ClassOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
												parUpdatedB = true;
												parUpdatedTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","ClassOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parUpdatedB = true;
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","ClassOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parUpdatedTypeB = true;
											}
											if(parUpdatedB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","ClassOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
											}
											if(parUpdatedTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","ClassOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
							oprUpdatedB = true;
							oprUpdatedRTB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprUpdatedB = true;
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprUpdatedRTB = true;
						}
						if(oprUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(oprUpdatedRTB == false && operacion.has("ReturnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("ReturnType", oprReturnType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
					}
				}
				
				if(objClasses.has("interfaceRealization")) {
					
					JSONArray interfaceArr = objClasses.getJSONArray("interfaceRealization");
					
					for(int i = 0; i < interfaceArr.length(); i++) {
						
						JSONObject interfaceObj = interfaceArr.getJSONObject(i);
						
						String idInterface = interfaceObj.getString("id");
						String supplier = interfaceObj.getString("supplier");
						String contract = interfaceObj.getString("contract");
						String client = interfaceObj.getString("client");
						
						Boolean idInterfaceB = false;
						Boolean contractB = false;
						Boolean supplierB = false;
						Boolean clientB = false;
						
						//Find interfaceRealization in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("interfaceRealization")) {	
							
							JSONArray interfaceArr2 = objClasses2.getJSONArray("interfaceRealization");
							
							for(int k3 = 0; k3 < interfaceArr2.length(); k3++) {
								
								JSONObject interfaceObj2 = interfaceArr2.getJSONObject(k3);
								
								//Find the id of the interface

								if(idInterface.equals(interfaceObj2.getString("id"))) {
									
									idInterfaceB = true;
									
									if(contract.equals(interfaceObj2.getString("contract"))) {
										contractB = true;
									}
									if(client.equals(interfaceObj2.getString("client"))) {
										clientB = true;
									}
									if(supplier.equals(interfaceObj2.getString("supplier"))) {
										supplierB = true;
									}
									
								}
								
							}}}
						
						if(idInterfaceB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","ClassInterfaceRealization");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							contractB = true;
							clientB = true;
							supplierB = true;
						}
						if(contractB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassInterfaceRealizationContract");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(clientB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassInterfaceRealizationClient");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(supplierB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","ClassInterfaceRealizationSupplier");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						
					}
				}
			}
			

		}
		
		//Search for created classes
		
		for(int i=0; i<fileDiff.length();i++) {
			
			//Process the classes
				
				JSONObject objClasses = fileDiff.getJSONObject(i);
				
				String id = objClasses.getString("id");
				String className = objClasses.getString("name");
				encontrado = false;
				
				//Find the class in the first file
				
				for(int k = 0; k < arrClasses.length(); k++) {
					
					JSONObject objClasses2 =  arrClasses.getJSONObject(k);
					
					if(id.equals(objClasses2.getString("id"))) {
						encontrado = true;
					}
				}
				
				//If the class is not found
				
				//If the class is an association-class
				
				if(objClasses.has("source") && encontrado==false) {
					JSONObject diferencia = new JSONObject();
					diferencia.put("OperationType", "Create");
					diferencia.put("ElementType", "AssociationClass");
					diferencia.put("ElementID", id);
					diferencia.put("ElementName", className);
					diferencias.put(diferencia);
				}else if(encontrado == false) {
					JSONObject diferencia = new JSONObject();
					diferencia.put("OperationType", "Create");
					diferencia.put("ElementType", "Class");
					diferencia.put("ElementID", id);
					diferencia.put("ElementName", className);
					diferencias.put(diferencia);
				}
				
				else {
					if(objClasses.has("attributes")) {
						
						JSONArray atributos = objClasses.getJSONArray("attributes");
						
						for(int j = 0; j < atributos.length(); j++) {
							
							JSONObject atributo = atributos.getJSONObject(j);
							String atrID = atributo.getString("id");
							String atrName = atributo.getString("name");
							String atrType = "";
							if(atributo.has("type")) atrType = atributo.getString("type");
							Boolean atrIDB = false;
							Boolean atrNameB = false;
							Boolean atrTypeB = false;
							
							//Find attributes in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("attributes") && id.equals(objClasses2.getString("id"))) {	
								
								JSONArray atributos2 = objClasses2.getJSONArray("attributes");
								
								for(int k3 = 0; k3 < atributos2.length(); k3++) {
									
									JSONObject atributo2 = atributos2.getJSONObject(k3);
									
									if(atrID.equals(atributo2.getString("id"))) {
										
										atrIDB = true;
										
										if(atributo.has("name")) {
											if(atributo2.has("name")) {
												atrNameB = true;
											}
										}
										
										if(atributo.has("type")) {
											
											if(atributo2.has("type")) {
											atrTypeB = true;
											}
										}
									}
								}
								}
								}
							if(atrIDB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassAttribute");
								diferencia.put("AttributeName", atrName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
								atrNameB = true;
								atrTypeB = true;
							}
							if(atrNameB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassAttributeName");
								diferencia.put("AttributeName", atrName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							if(atrTypeB == false && atributo.has("type")) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassAttributeType");
								diferencia.put("AttributeName", atrName);
								diferencia.put("AttributeType", atrType);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							
						}
					}
					
					//Check operations
					
					if(objClasses.has("operations")) {
						
						JSONArray operaciones = objClasses.getJSONArray("operations");
						
						for(int j = 0; j < operaciones.length(); j++) {
							
							JSONObject operacion = operaciones.getJSONObject(j);
							String opID = operacion.getString("id");
							String oprName = operacion.getString("name");
							String oprReturnType = "";
							String parName = "";
							JSONArray parametros = new JSONArray();
							if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
							if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
							Boolean oprIDB = false;
							Boolean parIDB = false;
							Boolean oprNameB = false;
							Boolean oprReturnTypeB = false;
							Boolean parNameB = false;
							Boolean parTypeB = false;
							
							//Find operations in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("operations") && id.equals(objClasses2.getString("id"))) {
															
								JSONArray operaciones2 = objClasses2.getJSONArray("operations");
								
								for(int k3 = 0; k3 < operaciones2.length(); k3++) {
									
									JSONObject operacion2 = operaciones2.getJSONObject(k3);
									
									if(opID.equals(operacion2.getString("id"))) {
										oprIDB = true;
										
										if(operacion.has("name")) {
											if(operacion2.has("name")) {
												oprNameB = true;
											}
										}
										
										if(operacion.has("returnType")) {
											if(operacion2.has("returnType")) {
												oprReturnTypeB = true;
											}
										}
									}
									
									//Check operation name
									
									if(opID.equals(operacion2.getString("id"))) {
										
										//Operation parameters
										
										if(parametros.length()>0) {
											
											for(int k4 = 0; k4 < parametros.length(); k4++) {
												
												JSONObject parametro = parametros.getJSONObject(k4);
												
												parName = parametro.getString("name");
												String parType = "";
												if(parametro.has("type")) parType = parametro.getString("type");
												
												parNameB = false;
												
												if(!operacion2.has("parameters") || operacion2.getJSONArray("parameters").length()==0) {
													parIDB = false;
												}
												else {
													JSONArray parametros2 = operacion2.getJSONArray("parameters");
													
													for(int t = 0; t < parametros2.length(); t++) {
														JSONObject parametro2 = parametros2.getJSONObject(t);
														
														if(parametro.getString("id").equals(parametro2.getString("id"))) {
															parIDB = true;
															
															if(parametro.has("name")) {
																if(parametro2.has("name")) {
																	parNameB = true;
																}
															}
															
															if(parametro.has("type")) {
																if(parametro2.has("type")) {
																	parTypeB = true;
																}
															}
														}
														
													}
												}
												if(parIDB == false) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","ClassOperationParameter");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
													parNameB = true;
													parTypeB = true;
												}
												if(parNameB == false) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","ClassOperationParameterName");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
												}
												if(parTypeB == false && parametro.has("type")) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","ClassOperationParameterType");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ParameterType", parType);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
												}
											}
										}
										
									}
								}
								}
		
								}
							if(oprIDB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassOperation");
								diferencia.put("OperationName", oprName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
								oprReturnTypeB = true;
								parNameB = true;
								oprNameB = true;
							}
							if(oprNameB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassOperationName");
								diferencia.put("OperationName", oprName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							if(oprReturnTypeB == false && operacion.has("returnType")) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassOperationReturnType");
								diferencia.put("OperationName", oprName);
								diferencia.put("OperationReturnType", oprReturnType);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
						}
					}
					
					if(objClasses.has("interfaceRealization")) {
						
						JSONArray interfaceArr = objClasses.getJSONArray("interfaceRealization");
						
						for(int j = 0; j < interfaceArr.length(); j++) {
							
							JSONObject interfaceObj = interfaceArr.getJSONObject(j);
							
							String idInterface = interfaceObj.getString("id");
							Boolean idInterfaceB = false;
							
							//Find interfaceRealization in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("interfaceRealization")) {	
								
								JSONArray interfaceArr2 = objClasses2.getJSONArray("interfaceRealization");
								
								for(int k3 = 0; k3 < interfaceArr2.length(); k3++) {
									
									JSONObject interfaceObj2 = interfaceArr2.getJSONObject(k3);
									
									//Find the id of the interface

									if(idInterface.equals(interfaceObj2.getString("id"))) {
										idInterfaceB = true;
									}
								}
								}
							}
							
							if(idInterfaceB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","ClassInterfaceRealization");
								diferencia.put("InterfaceRealizationID", idInterface);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
						}
					}
				}
		}
		
		return diferencias;
	}
	
	public JSONArray getClasesDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrClasses = new JSONArray();
		if(obj.has("classes"))	arrClasses = obj.getJSONArray("classes");
		
		//Process the class
		
		for(int k = 0; k < arrClasses.length(); k++) {
			
			//Search for deleted classes
			
			JSONObject objClasses = arrClasses.getJSONObject(k);
			
			String id = objClasses.getString("id");
			String className = objClasses.getString("name");
			Boolean modificadoName = false;
			encontrado = false;
			
			//Find the class in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					
					//If the class has been found
					
					encontrado = true;
					
					//Check the class name
					
					if(className.equals(objClasses2.getString("name"))) {
						modificadoName = true;
					}
				}
			}
			
			//If the class is not found
			
			
			//If the class is an association-class
			
			if(objClasses.has("source") && encontrado==false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType", "Package_AssociationClass");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}else if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType", "Package_Class");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}
			else if(objClasses.has("source") && modificadoName==false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType", "Package_AssociationClassName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}else if(modificadoName == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType", "Package_ClassName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", className);
				diferencias.put(diferencia);
			}
			
			//If the class is found, check the rest of the elements
			
			else {
				
				//Check associations
				
				if(objClasses.has("source") && objClasses.has("target")) {
					
					JSONObject source = objClasses.getJSONObject("source");
					JSONObject target = objClasses.getJSONObject("target");
					
					String sourceID = source.getString("id");
					String sourceName = source.getString("name");
					String targetID = target.getString("id");
					String targetName = target.getString("name");
					Boolean modificadoSource = false;
					Boolean modificadoTarget = false;
					
					for(int i = 0; i < fileDiff.length(); i++) {
						
						JSONObject objClasses2 = fileDiff.getJSONObject(i);
						modificadoSource = false;
						modificadoTarget = false;
						
						if(objClasses2.has("source") && objClasses2.has("target")) {
							
							JSONObject source2 = objClasses2.getJSONObject("source");
							JSONObject target2 = objClasses2.getJSONObject("target");
							
							if(sourceID.equals(source2.getString("id")) && sourceName.equals(source2.getString("name"))) {
								
								modificadoSource = true;
								
							}
							
							if(targetID.equals(target2.getString("id")) && targetName.equals(target2.getString("name"))) {
								
								modificadoTarget = true;
								
							}
						}
					}
					
					if(modificadoSource == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType", "Package_AssociationClassSource");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", className);
						diferencias.put(diferencia);
					}
					if(modificadoTarget == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType", "Package_AssociationClassTarget");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", className);
						diferencias.put(diferencia);
					}
				}
				
				if(objClasses.has("attributes")) {
					
					JSONArray atributos = objClasses.getJSONArray("attributes");
					
					for(int i = 0; i < atributos.length(); i++) {
						
						JSONObject atributo = atributos.getJSONObject(i);
						String atrID = atributo.getString("id");
						String atrName = atributo.getString("name");
						String atrType = "";
						if(atributo.has("type")) atrType=atributo.getString("type");
						Boolean atrIDB = false;
						Boolean atrNameB = false;
						Boolean atrUpdatedB = false;
						Boolean atrTypeB = false;
						Boolean atrUpdatedTypeB = false;
						
						//Check attributes in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("attributes") && id.equals(objClasses2.getString("id"))) {	
							
							JSONArray atributos2 = objClasses2.getJSONArray("attributes");
							
							for(int k3 = 0; k3 < atributos2.length(); k3++) {
								
								JSONObject atributo2 = atributos2.getJSONObject(k3);
								
								if(atrID.equals(atributo2.getString("id"))) {
									
									atrIDB = true;
									
									if(atributo2.has("name")){
										
										atrNameB = true;
									
										if(atrName.equals(atributo2.getString("name"))) {
											
											atrUpdatedB = true;
											
										}
											
										if(atributo2.has("type")) {
												
											atrTypeB = true;
										
											if(atrType.equals(atributo2.getString("type"))) {
													atrUpdatedTypeB = true;
												}
											}
									}
								}
							}
							}
							}
						
						if(atrIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassAttribute");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrNameB = true;
							atrTypeB = true;
							atrUpdatedB = true;
							atrUpdatedTypeB = true;
						}
						if(atrNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrTypeB = true;
							atrUpdatedB = true;
						}
						if(atrTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrUpdatedTypeB = true;
						}
						if(atrUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassAttributeName");
							diferencia.put("AttributeName", atrName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(atrUpdatedTypeB == false && atributo.has("type")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassAttributeType");
							diferencia.put("AttributeName", atrName);
							diferencia.put("AttributeType", atrType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							atrTypeB = true;
						}
						
					}
				}
				
				//Check operations
				
				if(objClasses.has("operations") && !objClasses.getJSONArray("operations").isEmpty()) {
					
					JSONArray operaciones = objClasses.getJSONArray("operations");
					
					for(int i = 0; i < operaciones.length(); i++) {
						
						JSONObject operacion = operaciones.getJSONObject(i);
						String opID = operacion.getString("id");
						String oprName = operacion.getString("name");
						String oprReturnType = "";
						String parName = "";
						
						JSONArray parametros = new JSONArray();
						if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
						if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
						
						Boolean oprIDB = false;
						Boolean parIDB = false;
						Boolean oprNameB = false;
						Boolean oprReturnTypeB = false;
						Boolean parNameB = false;
						Boolean parTypeB = false;
						Boolean oprUpdatedB = false;
						Boolean oprUpdatedRTB = false;
						Boolean parUpdatedB = false;
						Boolean parUpdatedTypeB = false;
						
						//Find operations in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("operations") && id.equals(objClasses2.getString("id"))) {
								
							JSONArray operaciones2 = objClasses2.getJSONArray("operations");
							
							for(int k3 = 0; k3 < operaciones2.length(); k3++) {
								
								JSONObject operacion2 = operaciones2.getJSONObject(k3);
								
								if(opID.equals(operacion2.getString("id"))) {
									oprIDB = true;
									
									if(operacion2.has("name")) {
										
										oprNameB = true;
										
										if(oprName.equals(operacion2.getString("name"))) {
											oprUpdatedB = true;
										}
									}
									
									if(operacion.has("returnType")) {
										if(operacion2.has("returnType")) {
											oprReturnTypeB = true;
											
											if(oprReturnType.equals(operacion2.getString("returnType"))) {
												oprUpdatedRTB = true;
											}
										}
									}
								}
								
								if(opID.equals(operacion2.getString("id"))) {
									
									//Operation parameters
									
									if(!parametros.isEmpty()) {
										
										for(int k4 = 0; k4 < parametros.length(); k4++) {
											
											JSONObject parametro = parametros.getJSONObject(k4);
											
											String parID = parametro.getString("id");
											parName = parametro.getString("name");
											String parType = "";
											if(parametro.has("type")) parType = parametro.getString("type");
											parIDB = false;
											parNameB = false;
											
											if(!operacion2.has("parameters")) {
												parIDB = false;
											}else {
												
												JSONArray parametros2 = operacion2.getJSONArray("parameters");
												
												for(int j = 0; j < parametros2.length(); j++) {
													
													JSONObject parametro2 = parametros2.getJSONObject(j);
													
													if(parID.equals(parametro2.getString("id"))) {
														parIDB = true;
														
														if(parametro.has("name")) {
															if(parametro2.has("name")) {
																parNameB = true;
																
																if(parName.equals(parametro2.getString("name"))) {
																	parUpdatedB = true;
																}
															}
														}
														
														if(parametro.has("type")) {
															if(parametro2.has("type")) {
																parTypeB = true;
																
																if(parType.equals(parametro2.getString("type"))) {
																	parUpdatedTypeB = true;
																}
															}
														}
													}
													
												}
											}
											if(parIDB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_ClassOperationParameter");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parNameB = true;
												parTypeB = true;
												parUpdatedB = true;
												parUpdatedTypeB = true;
											}
											if(parNameB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_ClassOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parUpdatedB = true;
											}
											if(parTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Delete");
												diferencia.put("ElementType","Package_ClassOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												parUpdatedTypeB = true;
											}
											if(parUpdatedB == false) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","Package_ClassOperationParameterName");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
											}
											if(parUpdatedTypeB == false && parametro.has("type")) {
												JSONObject diferencia = new JSONObject();
												diferencia.put("OperationType", "Update");
												diferencia.put("ElementType","Package_ClassOperationParameterType");
												diferencia.put("ParameterName", parName);
												diferencia.put("OperationName", oprName);
												diferencia.put("ParameterType", parType);
												diferencia.put("ElementID", id);
												diferencia.put("ElementName", className);
												diferencias.put(diferencia);
												
											}
										}
									}
									
								}
							}
							}
	
							}
						if(oprIDB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassOperation");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprNameB = true;
							oprReturnTypeB = true;
							parNameB = true;
							oprUpdatedB = true;
						}
						if(oprNameB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprUpdatedB = true;
						}
						if(oprReturnTypeB == false && operacion.has("returnType")) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("OperationReturnType", oprReturnType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							oprUpdatedRTB = true;
						}
						if(oprUpdatedB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassOperationName");
							diferencia.put("OperationName", oprName);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(oprUpdatedRTB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassOperationReturnType");
							diferencia.put("OperationName", oprName);
							diferencia.put("ReturnType", oprReturnType);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						
						
					}
				}
				
				if(objClasses.has("interfaceRealization")) {
					
					JSONArray interfaceArr = objClasses.getJSONArray("interfaceRealization");
					
					for(int i = 0; i < interfaceArr.length(); i++) {
						
						JSONObject interfaceObj = interfaceArr.getJSONObject(i);
						
						String idInterface = interfaceObj.getString("id");
						String supplier = interfaceObj.getString("supplier");
						String contract = interfaceObj.getString("contract");
						String client = interfaceObj.getString("client");
						
						Boolean idInterfaceB = false;
						Boolean contractB = false;
						Boolean supplierB = false;
						Boolean clientB = false;
						
						//Find interfaceRealization in the second file
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
							
							if(objClasses2.has("interfaceRealization")) {	
							
							JSONArray interfaceArr2 = objClasses2.getJSONArray("interfaceRealization");
							
							for(int k3 = 0; k3 < interfaceArr2.length(); k3++) {
								
								JSONObject interfaceObj2 = interfaceArr2.getJSONObject(k3);
								
								//Find the id of the interface

								if(idInterface.equals(interfaceObj2.getString("id"))) {
									
									idInterfaceB = true;
									
									if(contract.equals(interfaceObj2.getString("contract"))) {
										contractB = true;
									}
									if(client.equals(interfaceObj2.getString("client"))) {
										clientB = true;
									}
									if(supplier.equals(interfaceObj2.getString("supplier"))) {
										supplierB = true;
									}	
								}
							}
							}
						}
						
						if(idInterfaceB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_ClassInterfaceRealization");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
							contractB = true;
							clientB = true;
							supplierB = true;
						}
						if(contractB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassInterfaceRealizationContract");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(clientB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassInterfaceRealizationClient");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						if(supplierB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Update");
							diferencia.put("ElementType","Package_ClassInterfaceRealizationSupplier");
							diferencia.put("InterfaceRealizationID", idInterface);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", className);
							diferencias.put(diferencia);
						}
						
					}
				}
			}
		}
		
		//Search for created classes
		
		for(int i=0; i<fileDiff.length();i++) {
			
			//Process classes
				
				JSONObject objClasses = fileDiff.getJSONObject(i);
				
				String id = objClasses.getString("id");
				String className = objClasses.getString("name");
				encontrado = false;
				
				//Find class in the first file
				
				for(int k = 0; k < arrClasses.length(); k++) {
					
					JSONObject objClasses2 =  arrClasses.getJSONObject(k);
					
					if(id.equals(objClasses2.getString("id"))) {
						encontrado = true;
					}
				}
				
				//If the class is not found
				
				//If the class is an association-class
				
				if(objClasses.has("source") && encontrado==false) {
					JSONObject diferencia = new JSONObject();
					diferencia.put("OperationType", "Create");
					diferencia.put("ElementType", "Package_AssociationClass");
					diferencia.put("ElementID", id);
					diferencia.put("ElementName", className);
					diferencias.put(diferencia);
				}else if(encontrado == false) {
					JSONObject diferencia = new JSONObject();
					diferencia.put("OperationType", "Create");
					diferencia.put("ElementType", "Package_Class");
					diferencia.put("ElementID", id);
					diferencia.put("ElementName", className);
					diferencias.put(diferencia);
				}
				
				else {
					if(objClasses.has("attributes")) {
						
						JSONArray atributos = objClasses.getJSONArray("attributes");
						
						for(int j = 0; j < atributos.length(); j++) {
							
							JSONObject atributo = atributos.getJSONObject(j);
							String atrID = atributo.getString("id");
							String atrName = atributo.getString("name");
							String atrType = "";
							if(atributo.has("type")) atrType = atributo.getString("type");
							Boolean atrIDB = false;
							Boolean atrNameB = false;
							Boolean atrTypeB = false;
							
							//Find attributes in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("attributes") && id.equals(objClasses2.getString("id"))) {	
								
								JSONArray atributos2 = objClasses2.getJSONArray("attributes");
								
								for(int k3 = 0; k3 < atributos2.length(); k3++) {
									
									JSONObject atributo2 = atributos2.getJSONObject(k3);
									
									if(atrID.equals(atributo2.getString("id"))) {
										
										atrIDB = true;
										
										if(atributo.has("name")) {
											if(atributo2.has("name")) {
												atrNameB = true;
											}
										}
										
										if(atributo.has("type")) {
											
											if(atributo2.has("type")) {
											atrTypeB = true;
											}
										}
									}
								}
								}
		
								}
							if(atrIDB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassAttribute");
								diferencia.put("AttributeName", atrName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
								atrNameB = true;
								atrTypeB = true;
							}
							if(atrNameB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassAttributeName");
								diferencia.put("AttributeName", atrName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							if(atrTypeB == false && atributo.has("type")) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassAttributeType");
								diferencia.put("AttributeName", atrName);
								diferencia.put("AttributeType", atrType);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							
						}
					}
					
					//Check operations
					
					if(objClasses.has("operations")) {
						
						JSONArray operaciones = objClasses.getJSONArray("operations");
						
						for(int j = 0; j < operaciones.length(); j++) {
							
							JSONObject operacion = operaciones.getJSONObject(j);
							String opID = operacion.getString("id");
							String oprName = operacion.getString("name");
							String oprReturnType = "";
							String parName = "";
							JSONArray parametros = new JSONArray();
							if(operacion.has("returnType")) oprReturnType = operacion.getString("returnType");
							if(operacion.has("parameters")) parametros = operacion.getJSONArray("parameters");
							Boolean oprIDB = false;
							Boolean parIDB = false;
							Boolean oprNameB = false;
							Boolean oprReturnTypeB = false;
							Boolean parNameB = false;
							Boolean parTypeB = false;
							
							//Find operations in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("operations") && id.equals(objClasses2.getString("id"))) {
															
								JSONArray operaciones2 = objClasses2.getJSONArray("operations");
								
								for(int k3 = 0; k3 < operaciones2.length(); k3++) {
									
									JSONObject operacion2 = operaciones2.getJSONObject(k3);
									
									if(opID.equals(operacion2.getString("id"))) {
										oprIDB = true;
										
										if(operacion.has("name")) {
											if(operacion2.has("name")) {
												oprNameB = true;
											}
										}
										
										if(operacion.has("returnType")) {
											if(operacion2.has("returnType")) {
												oprReturnTypeB = true;
											}
										}
									}
									
									//Check the operation name
									
									if(opID.equals(operacion2.getString("id"))) {
										
										//Operation parameters
										
										if(parametros.length()>0) {
											
											for(int k4 = 0; k4 < parametros.length(); k4++) {
												
												JSONObject parametro = parametros.getJSONObject(k4);
												
												parName = parametro.getString("name");
												String parType = "";
												if(parametro.has("type")) parType = parametro.getString("type");
												
												parNameB = false;
												
												if(!operacion2.has("parameters") || operacion2.getJSONArray("parameters").length()==0) {
													parIDB = false;
												}
												else {
													JSONArray parametros2 = operacion2.getJSONArray("parameters");
													
													for(int t = 0; t < parametros2.length(); t++) {
														JSONObject parametro2 = parametros2.getJSONObject(t);
														
														if(parametro.getString("id").equals(parametro2.getString("id"))) {
															parIDB = true;
															
															if(parametro.has("name")) {
																if(parametro2.has("name")) {
																	parNameB = true;
																}
															}
															
															if(parametro.has("type")) {
																if(parametro2.has("type")) {
																	parTypeB = true;
																}
															}
														}
														
													}
												}
												if(parIDB == false) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","Package_ClassOperationParameter");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
													parNameB = true;
													parTypeB = true;
												}
												if(parNameB == false) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","Package_ClassOperationParameterName");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
												}
												if(parTypeB == false && parametro.has("type")) {
													JSONObject diferencia = new JSONObject();
													diferencia.put("OperationType", "Create");
													diferencia.put("ElementType","Package_ClassOperationParameterType");
													diferencia.put("ParameterName", parName);
													diferencia.put("OperationName", oprName);
													diferencia.put("ParameterType", parType);
													diferencia.put("ElementID", id);
													diferencia.put("ElementName", className);
													diferencias.put(diferencia);
												}
											}
										}
										
									}
								}
								}
		
								}
							if(oprIDB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassOperation");
								diferencia.put("OperationName", oprName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
								oprReturnTypeB = true;
								parNameB = true;
							}
							if(oprNameB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassOperationName");
								diferencia.put("OperationName", oprName);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							if(oprReturnTypeB == false && operacion.has("returnType")) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassOperationReturnType");
								diferencia.put("OperationName", oprName);
								diferencia.put("OperationReturnType", oprReturnType);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
							
							
						}
					}
					
					if(objClasses.has("interfaceRealization")) {
						
						JSONArray interfaceArr = objClasses.getJSONArray("interfaceRealization");
						
						for(int j = 0; j < interfaceArr.length(); j++) {
							
							JSONObject interfaceObj = interfaceArr.getJSONObject(j);
							
							String idInterface = interfaceObj.getString("id");
							Boolean idInterfaceB = false;
							
							//Find interfaceRealization in the first file
							
							for(int k2 = 0; k2 < arrClasses.length(); k2++) {
								
								JSONObject objClasses2 =  arrClasses.getJSONObject(k2);
								
								if(objClasses2.has("interfaceRealization")) {	
								
								JSONArray interfaceArr2 = objClasses2.getJSONArray("interfaceRealization");
								
								for(int k3 = 0; k3 < interfaceArr2.length(); k3++) {
									
									JSONObject interfaceObj2 = interfaceArr2.getJSONObject(k3);
									
									//Find the id of the interface

									if(idInterface.equals(interfaceObj2.getString("id"))) {
										idInterfaceB = true;
									}
								}
								}
							}
							
							if(idInterfaceB == false) {
								JSONObject diferencia = new JSONObject();
								diferencia.put("OperationType", "Create");
								diferencia.put("ElementType","Package_ClassInterfaceRealization");
								diferencia.put("InterfaceRealizationID", idInterface);
								diferencia.put("ElementID", id);
								diferencia.put("ElementName", className);
								diferencias.put(diferencia);
							}
										
									
							
						}
					}
				}
			
		}
		
		return diferencias;
	}

}
