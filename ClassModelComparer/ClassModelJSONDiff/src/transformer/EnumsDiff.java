package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class EnumsDiff {
	
	public JSONArray getEnumsDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrEnums = new JSONArray();
		if(obj.has("enums")) arrEnums = obj.getJSONArray("enums");
		
		//Process enumerations
		
		for(int k = 0; k < arrEnums.length(); k++) {
			
			//Search for deleted enumerations
			
			JSONObject objEnum = arrEnums.getJSONObject(k);
			
			String id = objEnum.getString("id");
			String enumName = objEnum.getString("name");
			encontrado = false;
			
			//Find enumeration in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					encontrado = true;
					
					if(enumName.equals(objClasses2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the enumeration is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Enumeration");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
				modificado = true;
			}
			else if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","EnumerationName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
			}
			
			//If the enumeration is found, check the rest of elements
			
			else {
				if(objEnum.has("literals")) {
					
					JSONArray literales = objEnum.getJSONArray("literals");
					
					for(int i = 0; i < literales.length(); i++) {
						
						String literal = literales.getString(i);
						Boolean literalB = false;
						
						//Find literals is the second file
						
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objEnums2 =  fileDiff.getJSONObject(k2);
							
							if(objEnums2.has("literals")) {
							
							JSONArray literales2 = objEnums2.getJSONArray("literals");
							
							for(int k3 = 0; k3 < literales2.length(); k3++) {
								
								//Check literal name
								
								if(literal.equals(literales2.getString(k3))) {
									literalB = true;
								}
							}
							}
						}
						
						if(literalB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","EnumerationLiteral");
							diferencia.put("LiteralName", literal);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", enumName);
							diferencias.put(diferencia);
						}
	
						}
						
					}
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created enumerations
			
			JSONObject objEnum = fileDiff.getJSONObject(k);
			
			String id = objEnum.getString("id");
			String enumName = objEnum.getString("name");
			encontrado = false;
			
			//Find enumeration in the first file
			
			for(int k2 = 0; k2 < arrEnums.length(); k2++) {
				
				JSONObject objClasses2 =  arrEnums.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the enumeration is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Enumeration");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
			}
			
			//If the enumeration is found, check the rest of elements
			
			else {
				if(objEnum.has("literals")) {
					
					JSONArray literales = objEnum.getJSONArray("literals");
					
					for(int i = 0; i < literales.length(); i++) {
						
						String literal = literales.getString(i);
						Boolean literalB = false;
						
						//Find literal in the first file
						
						
						for(int k2 = 0; k2 < arrEnums.length(); k2++) {
							
							JSONObject objEnums2 =  arrEnums.getJSONObject(k2);
							
							if(objEnums2.has("literals") && id.equals(objEnums2.getString("id"))) {
							
							JSONArray literales2 = objEnums2.getJSONArray("literals");
							
							for(int k3 = 0; k3 < literales2.length(); k3++) {
								
								//Check literal name
								
								if(literal.equals(literales2.getString(k3))) {
									literalB = true;
								}
							}
							}
						}
						
						if(literalB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","EnumerationLiteral");
							diferencia.put("LiteralName", literal);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", enumName);
							diferencias.put(diferencia);
						}
	
						}
						
					}
			}
		}
		
		return diferencias;
	}
	
	public JSONArray getEnumsDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrEnums = new JSONArray();
		if(obj.has("enums")) arrEnums = obj.getJSONArray("enums");
		
		//Process enumerations
		
		for(int k = 0; k < arrEnums.length(); k++) {
			
			//Search for deleted enumerations
			
			JSONObject objEnum = arrEnums.getJSONObject(k);
			
			String id = objEnum.getString("id");
			String enumName = objEnum.getString("name");
			encontrado = false;
			
			//Find enumeration in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objClasses2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					encontrado = true;
					
					if(enumName.equals(objClasses2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the enumeration is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_Enumeration");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
				modificado = true;
			}
			if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_EnumerationName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
			}
			
			//If the enumeration is found, check the rest of elements
			
			else {
				if(objEnum.has("literals")) {
					
					JSONArray literales = objEnum.getJSONArray("literals");
					
					for(int i = 0; i < literales.length(); i++) {
						
						String literal = literales.getString(i);
						Boolean literalB = false;
						
						//Find literals is the second file
						
						
						for(int k2 = 0; k2 < fileDiff.length(); k2++) {
							
							JSONObject objEnums2 =  fileDiff.getJSONObject(k2);
							
							if(objEnums2.has("literals")) {
							
							JSONArray literales2 = objEnums2.getJSONArray("literals");
							
							for(int k3 = 0; k3 < literales2.length(); k3++) {
								
								//Check literal name
								
								if(literal.equals(literales2.getString(k3))) {
									literalB = true;
								}
							}
							}
						}
						
						if(literalB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Delete");
							diferencia.put("ElementType","Package_EnumerationLiteral");
							diferencia.put("LiteralName", literal);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", enumName);
							diferencias.put(diferencia);
						}
	
						}
						
					}
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created enumerations
			
			JSONObject objEnum = fileDiff.getJSONObject(k);
			
			String id = objEnum.getString("id");
			String enumName = objEnum.getString("name");
			encontrado = false;
			
			//Find enumeration in the first file
			
			for(int k2 = 0; k2 < arrEnums.length(); k2++) {
				
				JSONObject objClasses2 =  arrEnums.getJSONObject(k2);
				
				if(id.equals(objClasses2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the enumeration is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_Enumeration");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", enumName);
				diferencias.put(diferencia);
			}
			
			//If the enumeration is found, check the rest of elements
			
			else {
				if(objEnum.has("literals")) {
					
					JSONArray literales = objEnum.getJSONArray("literals");
					
					for(int i = 0; i < literales.length(); i++) {
						
						String literal = literales.getString(i);
						Boolean literalB = false;
						
						//Find literal in the first file
						
						
						for(int k2 = 0; k2 < arrEnums.length(); k2++) {
							
							JSONObject objEnums2 =  arrEnums.getJSONObject(k2);
							
							if(objEnums2.has("literals") && id.equals(objEnums2.getString("id"))) {
							
							JSONArray literales2 = objEnums2.getJSONArray("literals");
							
							for(int k3 = 0; k3 < literales2.length(); k3++) {
								
								//Check literal name
								
								if(literal.equals(literales2.getString(k3))) {
									literalB = true;
								}
							}
							}
						}
						
						if(literalB == false) {
							JSONObject diferencia = new JSONObject();
							diferencia.put("OperationType", "Create");
							diferencia.put("ElementType","Package_EnumerationLiteral");
							diferencia.put("LiteralName", literal);
							diferencia.put("ElementID", id);
							diferencia.put("ElementName", enumName);
							diferencias.put(diferencia);
						}
	
						}
						
					}
			}
		}
		
		return diferencias;
	}

}
