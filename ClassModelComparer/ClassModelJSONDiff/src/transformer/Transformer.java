package transformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Transformer {

	private JSONArray JSONFile;
	private JSONArray JSONFileDiff;
	private int PRETTY_PRINT_INDENT_FACTOR = 4;

	public Transformer(String xmi, String xmiDiff) {
		JSONFile = new JSONArray(xmi);
		JSONFileDiff = new JSONArray(xmiDiff);
	}

	public String transform(){
		
		JSONArray res = new JSONArray();
		JSONArray modelos = new JSONArray();
		JSONObject modelo = new JSONObject();
		
		ClasesDiff clasesDiff = new ClasesDiff();
		EnumsDiff enumsDiff = new EnumsDiff();
		InterfacesDiff interfacesDiff = new InterfacesDiff();
		AssoDiff assoDiff = new AssoDiff();
		DepDiff depDiff = new DepDiff();
		PrimDiff primDiff = new PrimDiff();
		PackDiff packDiff = new PackDiff();
		UsageDiff usageDiff = new UsageDiff();
		DataDiff dataDiff = new DataDiff();
		
		for(int i = 0; i<JSONFile.length(); i++) {
			
			JSONObject JSONObj = JSONFile.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					//Add second file
					
					if(obj.has("classes") || getModules(JSONFileDiff).has("classes")) {
						
						JSONArray clasesFile2 = clasesJSON2(JSONFileDiff);
						
						modelo.put("classes",clasesDiff.getClasesDiff(obj,clasesFile2));
						res.put(modelo);
						
					}
					
					if(obj.has("enums") || getModules(JSONFileDiff).has("enums")) {
						
						JSONArray enumsFile2 = enumsJSON2(JSONFileDiff);
						
						modelo.put("enums", enumsDiff.getEnumsDiff(obj,enumsFile2));
					}
					
					if(obj.has("interfaces") || getModules(JSONFileDiff).has("interfaces")) {
						
						JSONArray interfacesFile2 = interfacesJSON2(JSONFileDiff);
						
						modelo.put("interfaces", interfacesDiff.getInterfacesDiff(obj,interfacesFile2));
					}
					
					if(obj.has("associations") || getModules(JSONFileDiff).has("associations")) {
						
						JSONArray assoFile2 = assoJSON2(JSONFileDiff);
						
						modelo.put("associations", assoDiff.getAssoDiff(obj,assoFile2));
					}
					
					if(obj.has("dependency") || getModules(JSONFileDiff).has("dependency")) {
						
						JSONArray depFile2 = depJSON2(JSONFileDiff);
						
						modelo.put("dependency", depDiff.getDepDiff(obj,depFile2));
					}
					
					if(obj.has("primitiveType") || getModules(JSONFileDiff).has("primitiveType")) {
						
						JSONArray primFile2 = primJSON2(JSONFileDiff);
						
						modelo.put("primitiveType", primDiff.getPrimDiff(obj,primFile2));
					}
					
					if(obj.has("packages") || getModules(JSONFileDiff).has("packages")) {
						
						JSONArray packFile2 = packJSON2(JSONFileDiff);
						
						modelo.put("packages", packDiff.getPackDiff(obj,packFile2));
					}
					
					if(obj.has("usage") || getModules(JSONFileDiff).has("usage")) {
						
						JSONArray usageFile2 = usageJSON2(JSONFileDiff);
						
						modelo.put("usage", usageDiff.getUsageDiff(obj,usageFile2));
					}
					
					if(obj.has("dataType") || getModules(JSONFileDiff).has("dataType")) {
						
						JSONArray dataFile2 = dataJSON2(JSONFileDiff);
						
						modelo.put("dataType", dataDiff.getDataDiff(obj,dataFile2));
					}
				}
			}
		
		}
		
		return res.toString(PRETTY_PRINT_INDENT_FACTOR);
	}
	
	public JSONObject getModules(JSONArray JSONDiff) {
		JSONArray aux = new JSONArray();
		JSONObject res = new JSONObject();
		
		for(int i = 0; i<JSONDiff.length();i++) {
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				aux = JSONObj.getJSONArray("modules");
				res = aux.getJSONObject(0);
			}
		}
		
		return res;
	}
	
	public JSONArray clasesJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("classes")) {
					
					res = obj.getJSONArray("classes");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray enumsJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("enums")) {
					
					res = obj.getJSONArray("enums");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray interfacesJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("interfaces")) {
					
					res = obj.getJSONArray("interfaces");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray assoJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("associations")) {
					
					res = obj.getJSONArray("associations");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray depJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("dependency")) {
					
					res = obj.getJSONArray("dependency");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray primJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("primitiveType")) {
					
					res = obj.getJSONArray("primitiveType");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray packJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("packages")) {
					
					res = obj.getJSONArray("packages");
					
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray usageJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("usage")) {
					
					res = obj.getJSONArray("usage");
					}
				}
			}
		}

		return res;
	}
	
	public JSONArray dataJSON2(JSONArray JSONDiff) {
		
		JSONArray res = new JSONArray();
		
		for(int i = 0; i<JSONDiff.length(); i++) {
			
			JSONObject JSONObj = JSONDiff.getJSONObject(i);
			
			if(JSONObj.has("modules")) {
				
				JSONArray arr = JSONObj.getJSONArray("modules");
				
				for(int j = 0; j < arr.length(); j++) {
					
					JSONObject obj = arr.getJSONObject(j);
					
					if(obj.has("dataType")) {
					
					res = obj.getJSONArray("dataType");
					}
				}
			}
		}

		return res;
	}

}
