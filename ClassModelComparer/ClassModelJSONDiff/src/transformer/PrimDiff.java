package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class PrimDiff {
	
	public JSONArray getPrimDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrPrim = new JSONArray();
		if(obj.has("primitiveType")) arrPrim = obj.getJSONArray("primitiveType");
		
		//Process primitiveTypes
		
		for(int k = 0; k < arrPrim.length(); k++) {
			
			//Search for deleted primitiveTypes
			
			JSONObject objPrim = arrPrim.getJSONObject(k);
			
			String id = objPrim.getString("id");
			String name = objPrim.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find primitiveType in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objPrim2 =  fileDiff.getJSONObject(k2);
						
				if(id.equals(objPrim2.getString("id"))) {
					
					encontrado = true;
					
					if(name.equals(objPrim2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the primitiveType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","PrimitiveType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","PrimitiveTypeName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created primitiveTypes
			
			JSONObject objPrim = fileDiff.getJSONObject(k);
			
			String id = objPrim.getString("id");
			encontrado = false;
			
			//Find primitiveType in the first file
			
			for(int k2 = 0; k2 < arrPrim.length(); k2++) {
				
				JSONObject objPrim2 =  arrPrim.getJSONObject(k2);
						
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the primitiveType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","PrimitiveType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		return diferencias;
	}
	
	public JSONArray getPrimDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrPrim = new JSONArray();
		if(obj.has("primitiveType")) arrPrim = obj.getJSONArray("primitiveType");
		
		//Process primitiveTypes
		
		for(int k = 0; k < arrPrim.length(); k++) {
			
			//Search for deleted primitiveTypes
			
			JSONObject objPrim = arrPrim.getJSONObject(k);
			
			String id = objPrim.getString("id");
			String name = objPrim.getString("name");
			encontrado = false;
			modificado = false;
			
			//Find primitiveType in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objPrim2 =  fileDiff.getJSONObject(k2);
						
				if(id.equals(objPrim2.getString("id"))) {
					
					encontrado = true;
					
					if(name.equals(objPrim2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the primitiveType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_PrimitiveType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			if(modificado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_PrimitiveTypeName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created primitiveTypes
			
			JSONObject objPrim = fileDiff.getJSONObject(k);
			
			String id = objPrim.getString("id");
			encontrado = false;
			
			//Find primitiveType in the first file
			
			for(int k2 = 0; k2 < arrPrim.length(); k2++) {
				
				JSONObject objPrim2 =  arrPrim.getJSONObject(k2);
						
				if(id.equals(objPrim2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the primitiveType is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_PrimitiveType");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objPrim.getString("name"));
				diferencias.put(diferencia);
			}
		}
		
		return diferencias;
	}

}
