package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class InstanceDiff {
	
	public JSONArray getInstanceDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrInstance = new JSONArray();
		if(obj.has("instanceSpecification")) arrInstance = obj.getJSONArray("instanceSpecification");
		
		//Process instanceSpecification
		
		for(int k = 0; k < arrInstance.length(); k++) {
			
			//Search for deleted instanceSpecification
			
			JSONObject objInstance = arrInstance.getJSONObject(k);
			
			String id = objInstance.getString("id");
			String name = objInstance.getString("name");
			encontrado = false;
			
			//Find instanceSpecification in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objInstance2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objInstance2.getString("id"))) {
					encontrado = true;
					
					if(name.equals(objInstance2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the instanceSpecification is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","InstanceSpecification");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			if(modificado = false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","InstanceSpecificationName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for cerated instanceSpecification
			
			JSONObject objInstance = fileDiff.getJSONObject(k);
			
			String id = objInstance.getString("id");
			encontrado = false;
			
			//Find instanceSpecification in the first file
			
			for(int k2 = 0; k2 < arrInstance.length(); k2++) {
				
				JSONObject objInstance2 =  arrInstance.getJSONObject(k2);
				
				if(id.equals(objInstance2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the instanceSpecification is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","InstanceSpecification");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
			}
			
		}
		
		return diferencias;
	}
	
	public JSONArray getInstanceDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		Boolean modificado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrInstance = new JSONArray();
		if(obj.has("instanceSpecification")) arrInstance = obj.getJSONArray("instanceSpecification");
		
		//Process instanceSpecification
		
		for(int k = 0; k < arrInstance.length(); k++) {
			
			//Search for deleted instanceSpecification
			
			JSONObject objInstance = arrInstance.getJSONObject(k);
			
			String id = objInstance.getString("id");
			String name = objInstance.getString("name");
			encontrado = false;
			
			//Find instanceSpecification in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objInstance2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objInstance2.getString("id"))) {
					encontrado = true;
					
					if(name.equals(objInstance2.getString("name"))) {
						modificado = true;
					}
				}
			}
			
			//If the instanceSpecification is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_InstanceSpecification");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
				modificado = true;
			}
			if(modificado = false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Update");
				diferencia.put("ElementType","Package_InstanceSpecificationName");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created instanceSpecification
			
			JSONObject objInstance = fileDiff.getJSONObject(k);
			
			String id = objInstance.getString("id");
			encontrado = false;
			
			//Find instanceSpecification in the first file
			
			for(int k2 = 0; k2 < arrInstance.length(); k2++) {
				
				JSONObject objInstance2 =  arrInstance.getJSONObject(k2);
				
				if(id.equals(objInstance2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the instanceSpecification is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_InstanceSpecification");
				diferencia.put("ElementID", id);
				diferencia.put("ElementName", objInstance.getString("name"));
				diferencias.put(diferencia);
			}
			
		}
		
		return diferencias;
	}

}
