package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class UsageDiff {
	
	public JSONArray getUsageDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrUsage = new JSONArray();
		if(obj.has("usage")) arrUsage = obj.getJSONArray("usage");
		
		//Process usages
		
		for(int k = 0; k < arrUsage.length(); k++) {
			
			//Search for deleted usages
			
			JSONObject objUsage = arrUsage.getJSONObject(k);
			
			String id = objUsage.getString("id");
			encontrado = false;
			
			//Find usage in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objUsage2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objUsage2.getString("id"))) {
					
					encontrado = true;
					
				}
			}
			
			//If the usage is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Usage");
				diferencia.put("ElementID", id);
				if(objUsage.has("name")) diferencia.put("ElementName", objUsage.getString("name"));
				diferencias.put(diferencia);
			} 
			
			//If the usage is found, check the usage name
			
			else if(encontrado == true) {
				if(objUsage.has("name")) {
					
					String usageName = objUsage.getString("name");
					Boolean usageNameB = false;
					String supplier = objUsage.getString("supplier");
					String client = objUsage.getString("client");
					Boolean updatedNameB = false;
					Boolean supplierB = false;
					Boolean clientB = false;
					
					for(int k2 = 0; k2 < fileDiff.length(); k2++) {
						
						JSONObject objUsage2 =  fileDiff.getJSONObject(k2);
						
						if(id.equals(objUsage2.getString("id")) && objUsage2.has("name")) {
							
							usageNameB = true;
						
							if(usageName.equals(objUsage2.getString("name"))) {
								updatedNameB = true;
							}
						
						}
						
						if(id.equals(objUsage2.getString("id"))) {
							
							if(supplier.equals(objUsage2.getString("supplier"))) {
								supplierB = true;
							}
							if(client.equals(objUsage2.getString("client"))) {
								clientB = true;
							}
						
						}
					}
					
					if(usageNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Delete");
						diferencia.put("ElementType","UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
						updatedNameB = true;
					}
					if(updatedNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
					if(clientB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","UsageClient");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
					if(supplierB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","UsageSupplier");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created usages
			
			JSONObject objUsage = fileDiff.getJSONObject(k);
			
			String id = objUsage.getString("id");
			encontrado = false;
			
			//Find usage in the first file
			
			for(int k2 = 0; k2 < arrUsage.length(); k2++) {
				
				JSONObject objUsage2 =  arrUsage.getJSONObject(k2);
				
				if(id.equals(objUsage2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the usage is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Usage");
				diferencia.put("ElementID", id);
				if(objUsage.has("name")) diferencia.put("ElementName", objUsage.getString("name"));
				diferencias.put(diferencia);
			}
			
			//If the usage is found, check the usage name
			
			else if(encontrado == true) {
				if(objUsage.has("name")) {
					
					String usageName = objUsage.getString("name");
					Boolean usageNameB = false;
					
					for(int k2 = 0; k2 < arrUsage.length(); k2++) {
						
						JSONObject objUsage2 =  arrUsage.getJSONObject(k2);
						
						if(id.equals(objUsage2.getString("id")) && objUsage2.has("name")) {
							
						usageNameB = true;
						
						}
					}
					
					if(usageNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Create");
						diferencia.put("ElementType","UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		return diferencias;
	}
	
	public JSONArray getUsageDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrUsage = new JSONArray();
		if(obj.has("usage")) arrUsage = obj.getJSONArray("usage");
		
		//Process usages
		
		for(int k = 0; k < arrUsage.length(); k++) {
			
			//Search for deleted usages
			
			JSONObject objUsage = arrUsage.getJSONObject(k);
			
			String id = objUsage.getString("id");
			encontrado = false;
			
			//Find usage in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objUsage2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objUsage2.getString("id"))) {
					
					encontrado = true;
					
				}
			}
			
			//If the usage is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_Usage");
				diferencia.put("ElementID", id);
				if(objUsage.has("name")) diferencia.put("ElementName", objUsage.getString("name"));
				diferencias.put(diferencia);
			} 
			
			//If the usage is found, check the usage name
			
			else if(encontrado == true) {
				if(objUsage.has("name")) {
					
					String usageName = objUsage.getString("name");
					Boolean usageNameB = false;
					String supplier = objUsage.getString("supplier");
					String client = objUsage.getString("client");
					Boolean updatedNameB = false;
					Boolean supplierB = false;
					Boolean clientB = false;
					
					for(int k2 = 0; k2 < fileDiff.length(); k2++) {
						
						JSONObject objUsage2 =  fileDiff.getJSONObject(k2);
						
						if(id.equals(objUsage2.getString("id")) && objUsage2.has("name")) {
							
							usageNameB = true;
						
							if(usageName.equals(objUsage2.getString("name"))) {
								updatedNameB = true;
							}
						
						}
						
						if(id.equals(objUsage2.getString("id"))) {
							
							if(supplier.equals(objUsage2.getString("supplier"))) {
								supplierB = true;
							}
							if(client.equals(objUsage2.getString("client"))) {
								clientB = true;
							}
						
						}
						
						
					}
					
					if(usageNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Delete");
						diferencia.put("ElementType","Package_UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
						updatedNameB = true;
					}
					if(updatedNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
					if(clientB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_UsageClient");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
					if(supplierB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_UsageSupplier");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created usages
			
			JSONObject objUsage = fileDiff.getJSONObject(k);
			
			String id = objUsage.getString("id");
			encontrado = false;
			
			//Find usage in the first file
			
			for(int k2 = 0; k2 < arrUsage.length(); k2++) {
				
				JSONObject objUsage2 =  arrUsage.getJSONObject(k2);
				
				if(id.equals(objUsage2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the usage is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_Usage");
				diferencia.put("ElementID", id);
				if(objUsage.has("name")) diferencia.put("ElementName", objUsage.getString("name"));
				diferencias.put(diferencia);
			}
			
			//If the usage is found, check the usage name
			
			else if(encontrado == true) {
				if(objUsage.has("name")) {
					
					String usageName = objUsage.getString("name");
					Boolean usageNameB = false;
					
					for(int k2 = 0; k2 < arrUsage.length(); k2++) {
						
						JSONObject objUsage2 =  arrUsage.getJSONObject(k2);
						
						if(id.equals(objUsage2.getString("id")) && objUsage2.has("name")) {
							
						usageNameB = true;
						
						}
					}
					
					if(usageNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Create");
						diferencia.put("ElementType","Package_UsageName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objUsage.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		return diferencias;
	}

}
