package transformer;

import org.json.JSONArray;
import org.json.JSONObject;

public class DepDiff {
	
	public JSONArray getDepDiff (JSONObject obj, JSONArray fileDiff) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrDep = new JSONArray();
		if(obj.has("dependency")) arrDep = obj.getJSONArray("dependency");
		
		//Process the dependencies
		
		for(int k = 0; k < arrDep.length(); k++) {
			
			//Search for deleted dependencies
			
			JSONObject objDep = arrDep.getJSONObject(k);
			
			String id = objDep.getString("id");
			encontrado = false;
			
			//Find dependency in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objDep2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objDep2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dependency is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Dependency");
				diferencia.put("ElementID", id);
				if(objDep.has("name")) diferencia.put("ElementName", objDep.getString("name"));
				diferencias.put(diferencia);
			}
			
			//Check the dependency name
			
			else if(encontrado == true) {
				
				if(objDep.has("name")) {
					
					String depName = objDep.getString("name");
					String supplier = objDep.getString("supplier");
					String client = objDep.getString("client");
					Boolean depNameB = false;
					Boolean updateNameB = false;
					Boolean supplierB = false;
					Boolean clientB = false;
					
					for(int k2 = 0; k2 < fileDiff.length(); k2++) {
						
						JSONObject objDep2 =  fileDiff.getJSONObject(k2);
						
						if(id.equals(objDep2.getString("id")) && objDep2.has("name")) {
							
							depNameB = true;
							
							if(depName.equals(objDep2.getString("name"))) {
								updateNameB = true;
							}
							
						}
						
						if(id.equals(objDep2.getString("id"))) {
							
							//Check the rest of parameters
							
							if(supplier.equals(objDep2.getString("supplier"))) {
								
								supplierB = true;
								
							}
							
							if(client.equals(objDep2.getString("client"))) {
								
								clientB = true;
								
							}
							
						}
						
						
					}
					
					if(depNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Delete");
						diferencia.put("ElementType","DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
						updateNameB = true;
					}
					if(updateNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
					if(supplierB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","DependencySupplier");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
					if(clientB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","DependencyClient");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created dependencies
			
			JSONObject objDep = fileDiff.getJSONObject(k);
			
			String id = objDep.getString("id");
			encontrado = false;
			
			//Find the dependency in the first file
			
			for(int k2 = 0; k2 < arrDep.length(); k2++) {
				
				JSONObject objDep2 =  arrDep.getJSONObject(k2);
				
				if(id.equals(objDep2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dependency is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Dependency");
				diferencia.put("ElementID", id);
				if(objDep.has("name")) diferencia.put("ElementName", objDep.getString("name"));
				diferencias.put(diferencia);
			}
			
			//Check dependency name
			
			else if(encontrado == true) {
				if(objDep.has("name")) {
					
					String depName = objDep.getString("name");
					Boolean depNameB = false;
					
					for(int k2 = 0; k2 < arrDep.length(); k2++) {
						
						JSONObject objDep2 =  arrDep.getJSONObject(k2);
						
						if(id.equals(objDep2.getString("id")) && objDep2.has("name")) {
							
						depNameB = true;
						
						}
					}
					
					if(depNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Create");
						diferencia.put("ElementType","DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		return diferencias;
	}
	
	public JSONArray getDepDiff (JSONObject obj, JSONArray fileDiff, Boolean pack) {
		
		Boolean encontrado = false;
		JSONArray diferencias = new JSONArray();
		
		JSONArray arrDep = new JSONArray();
		if(obj.has("dependency")) arrDep = obj.getJSONArray("dependency");
		
		//Process dependencies
		
		for(int k = 0; k < arrDep.length(); k++) {
			
			//Search for deleted dependencies
			
			JSONObject objDep = arrDep.getJSONObject(k);
			
			String id = objDep.getString("id");
			encontrado = false;
			
			//Find dependency in the second file
			
			for(int k2 = 0; k2 < fileDiff.length(); k2++) {
				
				JSONObject objDep2 =  fileDiff.getJSONObject(k2);
				
				if(id.equals(objDep2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dependency is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Delete");
				diferencia.put("ElementType","Package_Dependency");
				diferencia.put("ElementID", id);
				if(objDep.has("name")) diferencia.put("ElementName", objDep.getString("name"));
				diferencias.put(diferencia);
			}
			
			//Check dependency name
			
			else if(encontrado == true) {
				
				if(objDep.has("name")) {
					
					String depName = objDep.getString("name");
					String supplier = objDep.getString("supplier");
					String client = objDep.getString("client");
					Boolean depNameB = false;
					Boolean updateNameB = false;
					Boolean supplierB = false;
					Boolean clientB = false;
					
					for(int k2 = 0; k2 < fileDiff.length(); k2++) {
						
						JSONObject objDep2 =  fileDiff.getJSONObject(k2);
						
						if(id.equals(objDep2.getString("id")) && objDep2.has("name")) {
							
							depNameB = true;
							
							if(depName.equals(objDep2.getString("name"))) {
								updateNameB = true;
							}
							
						}
						
						if(id.equals(objDep2.getString("id"))) {
							
							//Check the rest of parameters
							
							if(supplier.equals(objDep2.getString("supplier"))) {
								
								supplierB = true;
								
							}
							
							if(client.equals(objDep2.getString("client"))) {
								
								clientB = true;
								
							}
							
						}
					}
					
					if(depNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Delete");
						diferencia.put("ElementType","Package_DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
						updateNameB = true;
					}
					if(updateNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
					if(supplierB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_DependencySupplier");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
					if(clientB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Update");
						diferencia.put("ElementType","Package_DependencyClient");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
		}
		
		for(int k = 0; k < fileDiff.length(); k++) {
			
			//Search for created dependencies
			
			JSONObject objDep = fileDiff.getJSONObject(k);
			
			String id = objDep.getString("id");
			encontrado = false;
			
			//Find dependency in the first file
			
			for(int k2 = 0; k2 < arrDep.length(); k2++) {
				
				JSONObject objDep2 =  arrDep.getJSONObject(k2);
				
				if(id.equals(objDep2.getString("id"))) {
					encontrado = true;
				}
			}
			
			//If the dependency is not found

			if(encontrado == false) {
				JSONObject diferencia = new JSONObject();
				diferencia.put("OperationType", "Create");
				diferencia.put("ElementType","Package_Dependency");
				diferencia.put("ElementID", id);
				if(objDep.has("name")) diferencia.put("ElementName", objDep.getString("name"));
				diferencias.put(diferencia);
			}
			
			//Check dependency name
			
			else if(encontrado == true) {
				if(objDep.has("name")) {
					
					String depName = objDep.getString("name");
					Boolean depNameB = false;
					
					for(int k2 = 0; k2 < arrDep.length(); k2++) {
						
						JSONObject objDep2 =  arrDep.getJSONObject(k2);
						
						if(id.equals(objDep2.getString("id")) && objDep2.has("name")) {
							
						depNameB = true;
						
						}
					}
					
					if(depNameB == false) {
						JSONObject diferencia = new JSONObject();
						diferencia.put("OperationType", "Create");
						diferencia.put("ElementType","Package_DependencyName");
						diferencia.put("ElementID", id);
						diferencia.put("ElementName", objDep.getString("name"));
						diferencias.put(diferencia);
					}
				}
			}
			
		}
		
		return diferencias;
	}

}
