package main;

import java.io.File;
import java.io.IOException;

import org.json.JSONObject;

import inputoutput.TextFile;
import transformer.Transformer;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		String xmi,xmiDiff,workingDir,fileURL;		
		
		workingDir = System.getProperty("user.dir");
		String fileName=workingDir+File.separator+args[0];
		xmi=TextFile.readTextFile(fileName);
			
		String fileNameDiff=workingDir+File.separator+args[1];
		xmiDiff=TextFile.readTextFile(fileNameDiff);        
		
        Transformer tr=new Transformer (xmi,xmiDiff);
        String json=tr.transform();
        
        if(Config.debugMode){
        	System.out.println(json);
        	String targetFile = fileURL.substring(0,fileURL.lastIndexOf("."))+".json";
        	TextFile.writeTextFile(json, targetFile);
        }else{
        	String targetFile=workingDir+File.separator+args[0].substring(0, args[0].lastIndexOf("."))+"Diff.json";
        	TextFile.writeTextFile(json, targetFile);
        }	                        

	}

}
