package main;

public class Config {
	public static final boolean debugMode=false;
	public static final boolean includeElementVisibility=false;
	public static final boolean includeElementType=false;
	public static final boolean simplifyAssociationEnds=true;
	public static final int PRETTY_PRINT_INDENT_FACTOR = 4;
}
