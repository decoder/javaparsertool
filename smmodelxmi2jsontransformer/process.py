# !/usr/bin/python
# -*- coding: utf-8 -*- 
import os
import sys
import json
import string
import argparse
import requests
import urllib3
import random
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def loadFileContent(fileName):
	with open(fileName, 'r') as file:
		data = file.read()
		return data

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	if username==None  and password==None:
		pload = {"user_name":"admin", "user_password":"cN6jK7eU5bM3"}
	else:
		pload = {"user_name":username, "user_password":password}
	url="/user/login"
	resStatus, resLogin=doPOST(url,str(pload).replace("'",'"'))
	key=(json.loads(resLogin))['key']
	return key



def doPOST(url, data, key=None):	
	r = requests.post('https://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('https://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('https://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text




PKM_API_HOST="pkm"

#START PARAMETERS
ap = argparse.ArgumentParser(description="Decoder StateMachineModel2JSON Process",add_help=True)
ap.add_argument("-k", "--key", required=True, metavar="login key")
ap.add_argument("-d", "--dbName", required=True, metavar="database name")
ap.add_argument("-s", "--sourceFileName", required=True, metavar="Source File name to parse")
ap.add_argument("-o", "--output", required=False, metavar="Output file log")
args = vars(ap.parse_args())

key=args['key']
dbName=args['dbName']
fileName=args['sourceFileName']
outputLogFile=args['output']
#END PARAMETERS


url="/uml/rawuml/"+dbName+"/"+fileName
status,document=doGET(url,key)
document=json.loads(document)

text_file = open(fileName, "wt")
text_file.write(document['content'])
text_file.close()


classModel2JSONCommand="java -jar SMModelXMI2JSON.jar "+fileName
os.system(classModel2JSONCommand)

data=loadFileContent(fileName.replace(".uml",".json"))
d=json.loads(data)
d['diagram']=""
data=json.dumps(d)
doPutAnswerCode, doPutAnswer = doPUT('/uml/uml_state_machine/'+dbName, data, key)

if makeOutputLogFile:
	outputLog = open(outputLogFile, "w")
	outputLog.write(json.dumps(dict(code=doPutAnswerCode,answer=doPutAnswer), indent=4))
	outputLog.close()
else:
	print(doPutAnswerCode,doPutAnswer)


rmCommand = "rm "+fileName+" "+fileName.replace(".uml",".json")
os.system(rmCommand)




