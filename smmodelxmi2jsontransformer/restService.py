#!flask/bin/python
from flask import Flask, jsonify, abort
import os, sys
from flask import request
import random, string, json

app = Flask(__name__)
app.debug = True
app.reloader = True

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

@app.route('/decoder/smModelToJson/<dbName>/<sourceFileName>', methods=['GET'])
def runSMModel2JSON(dbName, sourceFileName):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')

	output = "res_" + getRandomString(8)

	orderToRun = "python3 process.py --key " + key + " --dbName " + dbName + " --sourceFileName " + sourceFileName + " --output " + output
	os.system(orderToRun)
	f = open(output,'r')
	outputFileContent=json.load(f)
	if str(outputFileContent['code'])[0] != "2":
		os.system("rm " + output)
		abort(outputFileContent['code'])
	else:
		os.system("rm " + output)
		print(outputFileContent['answer'])
		return outputFileContent['answer']
	



		


