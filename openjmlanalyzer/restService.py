#!flask/bin/python
from flask import Flask, jsonify, abort
import os, sys
from flask import request
import random, string
import json
from requests.utils import quote
import subprocess

app = Flask(__name__)
app.debug = True
app.reloader = True
INV_ID=None

def printToLog(stringToPrint, initialize="a"):
	global INV_ID
	text_file = open("./logs/"+INV_ID+".log", initialize)
	text_file.write(stringToPrint+"\n")
	text_file.close()

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

@app.route('/decoder/openjml/<dbName>/<path:sourceFileName>', methods=['GET'])
def runASTExtractor(dbName, sourceFileName):
	global INV_ID
	if (not ('key' in request.headers)) or (not ("invocationID" in request.args)):
		abort(400)

	invocationID=request.args["invocationID"]
	key = request.headers.get('key')
	INV_ID = invocationID
	printToLog("", "w")

	generate=None
	if "generate" in request.args:
		if request.args['generate'] == "sc":
			generate ="esc"
		elif request.args['generate'] == "rac":
			generate ="rac"
		else:
			abort(400)
	
	orderToRun = "python3 process.py --key " + key + " --dbName " + dbName + " --sourceFileName " + quote(sourceFileName, safe='') +  " --invocationID " + invocationID
	if generate != None:
		orderToRun = orderToRun + " --generate " + generate
	
	printToLog(orderToRun)
	p = subprocess.Popen(orderToRun.split(" "))
	p.wait()

	output= "res_"+invocationID
	f = open(output,'r')
	outputFileContent=json.load(f)
	
	printToLog(json.dumps(outputFileContent, indent=4))
	
	if str(outputFileContent['code'])[0] != "2":
		#os.system("rm " + output)
		abort(outputFileContent['code'])
	else:
		#os.system("rm " + output)
		print(outputFileContent['answer'])
		return json.dumps(dict(answer=outputFileContent['answer'].replace('"',""), numberOfErrors=outputFileContent['numErr'], numberOfWarnings=outputFileContent['numWar']), indent=4)


