# !/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import json
import string
import argparse
import requests
import urllib3
import random
import shutil

from datetime import datetime

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def loadFileContent(fileName):
	with open(fileName, 'r') as file:
		data = file.read()
		return data

def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	if username==None  and password==None:
		pload = {"user_name":"admin", "user_password":"cN6jK7eU5bM3"}
	else:
		pload = {"user_name":username, "user_password":password}
	url="/user/login"
	resStatus, resLogin=doPOST(url,str(pload).replace("'",'"'))
	key=(json.loads(resLogin))['key']
	return key



def doPOST(url, data, key=None):
	r = requests.post('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def updateInvocation(invocationUpdate):
	global dbName, key
	getSt, getTx=doGET('/invocations/'+dbName+'/'+invocationUpdate['invocationID'], key)
	print(getSt,getTx)
	inv=json.loads(getTx)
	
	inv.update(invocationUpdate)
	doPUT('/invocations/'+dbName, json.dumps([inv]), key)
	print(json.dumps([inv], indent=4))

def sendLogAndFinishProcess(numErr, numWar, status, logMessages, warnings, errors, startTimestamp, resFileName, resFileNameParsed):
	global tool, dbName, key, outputLogFile, fileName, fileName2, invocationId
	formatoFecha="%a, %d %b %Y %H:%M:%S %Z%z"
	res=dict(tool="OpenJML "+tool, details=dict(numberOfErrors=numErr, numberOfWarings=numWar), status=status, type="Log", messages=logMessages, warnings=warnings, errors=errors)
	print(json.dumps(res, indent=4))
	res["nature of report"]="OpenJML Analysis report"
	res["start running time"]=startTimestamp.strftime(formatoFecha)
	res["end running time"]=datetime.now().strftime(formatoFecha)
	
	invocationUpdate=dict()
	invocationUpdate["invocationID"] = invocationId
	
	doPostAnswerCode,doPostAnswer = doPOST('/log/'+dbName, json.dumps([res]), key)
	print(doPostAnswerCode, doPostAnswer)
	if int(doPostAnswerCode)==201:
		postTextAsJSON=json.loads(doPostAnswer)
		if status:
			invocationUpdate["invocationStatus"]="COMPLETED"
		else:
			invocationUpdate["invocationStatus"]="FAILED"

		if len(postTextAsJSON)>0:
			newLogID=postTextAsJSON[0]
			invocationResults=[]
			invocationResults.append(dict(path="/log/"+dbName+"/"+str(newLogID),type="log"))
			invocationUpdate["invocationResults"]=invocationResults

		outputLog = open(outputLogFile, "w")
		outputLog.write(json.dumps(dict(code=doPostAnswerCode,answer=doPostAnswer, numErr=numErr, numWar=numWar), indent=4))
		outputLog.close()
		

		rmCommand = "rm " +fileName2
		if resFileName!=None:
			rmCommand+= " " + resFileName
		if resFileNameParsed!=None:
			rmCommand+= " " + resFileNameParsed
		#os.system(rmCommand)
		shutil.rmtree("/openjmlanalyzer/ojml_classpath/"+INV_ID, ignore_errors=True)

	else:
		invocationUpdate["invocationStatus"]="FAILED"

	invocationUpdate["timestampCompleted"]=datetime.now().strftime("%Y%m%d_%H%M%S")

	updateInvocation(invocationUpdate)


def downloadAllFiles(key):
	global INV_ID
	getStatus,document=doGET("/files/"+dbName+"/?type=Code",key)
	document=json.loads(document)
	shutil.rmtree("/openjmlanalyzer/ojml_classpath/"+INV_ID, ignore_errors=True)
	os.makedirs("/openjmlanalyzer/ojml_classpath/"+INV_ID, exist_ok=True)
	for entry in document:
		getStatusTemp,entryTemp=doGET("/files/" + dbName + "/" + entry['rel_path'].replace("/","%2F"), key)
		codeToSave=json.loads(entryTemp)['content']
		if "/" in entry['rel_path']:
			wheretoSave=os.path.join("/openjmlanalyzer/ojml_classpath/"+INV_ID,entry['rel_path'].rsplit("/",1)[1])
		else:
			wheretoSave=os.path.join("/openjmlanalyzer/ojml_classpath/"+INV_ID,entry['rel_path'])
		with open(wheretoSave, "w") as ftemp:
			ftemp.write(codeToSave)

def processError(r, filename):
	res='ERROR: '+filename+":"+str(r['line'])+" -> "+r['description']
	if 'moreInfo' in r:
		res=+" ("+r["moreInfo"]+")"
	return res


startTimestamp=datetime.now()

PKM_API_HOST="pkm"

#START PARAMETERS
ap = argparse.ArgumentParser(description="Decoder OpenJML Process",add_help=False)
ap.add_argument("-k", "--key", required=True, metavar="login key")
ap.add_argument("-d", "--dbName", required=False, metavar="database name", default="mythaistar")
ap.add_argument("-s", "--sourceFileName", required=True, metavar="Source File name to parse")
ap.add_argument("-g", "--generate", required=False, metavar="Generation Items [esc | rac]", choices=['esc', 'rac'])
ap.add_argument("-i", "--invocationID", required=True, metavar="The Invocation ID")
args = vars(ap.parse_args())

key=args['key']
dbName=args['dbName']
fileName=args['sourceFileName']
generate=args['generate']

invocationId=args['invocationID']
outputLogFile="res_"+invocationId
INV_ID = invocationId
#END PARAMETERS

option="typeChecking"
if generate == "rac":
	option="assertionChecking"
elif generate == "esc":
	option="staticChecking"


tool=option
logMessages=[]
status=True



fileName2=fileName.replace("%2F", "%2f")
if "%2f" in fileName2:
	fileName2=fileName2.rsplit("%2f",1)[1]

downloadedFileName=fileName.replace(".java","")

#downloadAllCodeFromPKM()

url="/code/rawsourcecode/"+dbName+"/"+fileName
getStatus,document=doGET(url,key)
if getStatus!=200:
	print("Error downloading file")
	logMessages.append("Error downloading file")
	status=False
	sendLogAndFinishProcess(0, 0, status, logMessages, [], [], startTimestamp, None, None)
	quit()

document=json.loads(document)
logMessages.append(fileName +" Downloaded Succesfully!")
text_file = open(fileName2, "wt")
text_file.write(document['content'])
text_file.close()

downloadAllFiles(key)

resFileName = "resultOpenJML_" + downloadedFileName
resFileNameParsed = "resultOpenJML_" + downloadedFileName + "_parsed.json"


openJMLCommand = "java -jar openjml/openjml.jar -Xlint:deprecation -cp /openjmlanalyzer/jars -dir /openjmlanalyzer/ojml_classpath/"+INV_ID+" -dir /openjmlanalyzer/my-thai-star-jml/java/mtsj/core/src/main/java -dir /openjmlanalyzer/my-thai-star-jml/java/mtsj/api/src/main/java -dir /openjmlanalyzer/my-thai-star-jml/java/mtsj/batch/src/main/java "

if generate != None:
	openJMLCommand = openJMLCommand + "-" + generate + " "
openJMLCommand = openJMLCommand + fileName2+" > " + resFileName
os.system(openJMLCommand)
logMessages.append("OpenJML Analysis Success!")

parserCommand = "python3 openJMLResultsParser.py --tool " + option + " --sourceFileName " + fileName2 + " --fileToParse " + resFileName + " > " + resFileNameParsed
os.system(parserCommand)
logMessages.append("OpenJML Results Parsed Success!")

data=loadFileContent(resFileNameParsed)

numErr=0
numWar=0
tool=""
errors=[]
warnings=[]
for d in json.loads(data):
	numErr+=d['totalNumberErrors']
	numWar+=d['totalNumberWarnings']
	tool=d['tool']
	for r in d['result']:
		if r['messageType'] == "Error":
			errors.append(processError(r, fileName2))
		else:
			warnings.append(json.dumps(r))

sendLogAndFinishProcess(numErr, numWar, status, logMessages, warnings, errors, startTimestamp, resFileName, resFileNameParsed)

