# !/usr/bin/python
# -*- coding: utf-8 -*- 

import os
import sys
import json
import random
import string
import argparse

def analyzeOpenedFileLines(lines):
	global numOfErrors, numOfWarnings, sourceFile
	resLine=lines[0]
	scLine=""
	if len(lines)>1:
		scLine=lines[1]
	moreInfo=None
	if len(lines)>2:
		i=2
		moreInfo=""
		while i < len(lines):
			moreInfo=moreInfo+lines[i]+"; "
			i+=1
		moreInfo=moreInfo[:-2]
	
	resLineParts = resLine.split(":")
	lineNumber=int(resLineParts[1])
	errType=resLineParts[2].strip()
	if "warning" in errType:
		messageType = "Warning"
		numOfWarnings = numOfWarnings + 1
	else:
		messageType = "Error"
		numOfErrors = numOfErrors + 1
	res=dict()
	res['line']=lineNumber
	res['messageType']=messageType
	res['sourceCode']=scLine.strip()
	if moreInfo:
		res['moreInfo']=moreInfo

	if resLineParts[3].lstrip().startswith("Associated declaration"):
		res['description'] = "Associated declaration"
		res['associatedDeclarationLine'] = int(resLineParts[5])
	else:
		desc=""
		for p in resLineParts[3:]:
			desc=desc+p+":"
		desc=desc[:-1]
		message=desc
		if "should be declared in a file named " in desc:
			cName=desc.split("should be declared in a file named ")[1]
			if cName==sourceFile:
				numOfErrors = numOfErrors-1
				return None
		res['description']=desc
	return res

#START PARAMETERS
ap = argparse.ArgumentParser(description="Decoder OpenJML Process",add_help=False)
ap.add_argument("-t", "--tool", required=True, metavar="Tool Used To perform the analysis [typeChecking | assertionChecking | staticChecking]", choices=['typeChecking', 'assertionChecking', 'staticChecking'])
ap.add_argument("-f", "--fileToParse", required=True, metavar="File to Parse")
ap.add_argument("-s", "--sourceFileName", required=True, metavar="Source File name to parse")
args = vars(ap.parse_args())

tool=args['tool']
fileToOpen=args['fileToParse']
sourceFile=args['sourceFileName']
#END PARAMETERS

line1=None
line2=None
line3=None

numOfErrors=0
numOfWarnings=0
result=[]

openedFile=open(fileToOpen, 'r')
openedFileLines=openedFile.readlines()
i=0
while i < len(openedFileLines):
	thisLine=openedFileLines[i].replace("\n","").strip()
	if(len(thisLine)>0):
		if thisLine.startswith(sourceFile):
			linesToAnalyze=[]
			linesToAnalyze.append(thisLine)
			i=i+1
			while i < len(openedFileLines):
				thisLine=openedFileLines[i].replace("\n","").strip()
				if len(thisLine)>0:
					if not thisLine.startswith(sourceFile):
						if thisLine.startswith("/") and not thisLine.startswith("//"):
							i=i-2
							break
						if len(thisLine.replace("^",""))>0:
							if not thisLine.startswith("openjml/openjml.jar"):
								linesToAnalyze.append(thisLine)
							else:
								i=i-2
								break
					else:
						i=i-2
						break
				else:
					i=i-2
					break
				i=i+1
			analysis=analyzeOpenedFileLines(linesToAnalyze)
			if analysis!=None:
				result.append(analysis)

	i=i+1
	
res=dict(totalNumberErrors=numOfErrors, totalNumberWarnings=numOfWarnings, result=result, tool=tool, sourceFile=sourceFile)
res2=[]
res2.append(res)
print(json.dumps(res2, indent=4))
